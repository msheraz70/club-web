import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Growl} from 'primereact/growl';

class create extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.addHub = this.addHub.bind(this);
    
    this.state = {
      fields: {
        hubNumber: "",
        hubName: "",
        hubAddress: "",
        hubPhone: "",
        hubEmail: "",
        hubAdmin: ""
      },
      errors: {}
    }

    this.otherRequiredFields = [
      "hubName","hubAddress"
    ]

  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    for(var i=0;i<this.otherRequiredFields.length;i++) {
      if(!fields[this.otherRequiredFields[i]]){
        formIsValid = false;
        errors[this.otherRequiredFields[i]] = "Required Field";
     }
    }

    //Email
    if(fields["hubEmail"] !== ""){
       let lastAtPos = fields["hubEmail"].lastIndexOf('@');
       let lastDotPos = fields["hubEmail"].lastIndexOf('.');

       if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["hubEmail"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["hubEmail"].length - lastDotPos) > 2)) {
          formIsValid = false;
          errors["hubEmail"] = "Email is not valid";
        }
   }  

   this.setState({errors: errors});
   return formIsValid;
}

showSuccess() {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: 'Hub Created Successfully !!'});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError() {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Hub Creation Failed !!'});
}

addHub(e) {
    e.preventDefault();
    if(this.handleValidation()){
      auth.postData('hub',this.state.fields).then(res => {
        if(res!==undefined) {
          if(res._id) {
            this.showSuccess();
            auth.getPicklistData('hub');
            window.location = '/#/hubs';
          }
        } else {
          this.showError();
        }
    });
   } else {
      this.showWarn();
   }
  }

  cancel() {
    window.location = '/#/hubs';
  }

  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    let errorList = Object.assign({}, this.state.errors);
    errorList[inputName] ="";
    let otherCopy = Object.assign({}, this.state.fields);
    otherCopy[inputName] = inputValue;
    this.setState({
    fields: otherCopy,errors:errorList
    });
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> Add Hub</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.addHub} noValidate autoComplete="off">
          <br/>
          <div className="card-header">HUB DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-4">
                <div>Hub Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Hub Name" name="hubName" id="in" value={this.state.fields.hubName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubName"]}</span>
              </div>

              <div className="p-col-4">
                <div>Hub Phone Number</div>
                <div className="p-inputgroup">
                  <InputMask mask="9999999999" name="hubPhone" value={this.state.fields.hubPhone} placeholder="999-99-9999" onChange={this.updateState}></InputMask>
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubPhone"]}</span>
              </div>

              <div className="p-col-4">
                <div>Hub Email ID</div>
                <div className="p-inputgroup">
                  <InputText keyfilter="email" name="hubEmail" value={this.state.fields.hubEmail} placeholder="abc@xyz.com" onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubEmail"]}</span>
              </div>

             
            </div>
            <br />
            
            <div className="p-grid p-fluid">

            <div className="p-col-12">
                <div>Hub Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <InputTextarea placeholder="Hub Address" name="hubAddress" rows={2} cols={30} value={this.state.fields.hubAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubAddress"]}</span>
              </div>

             

            </div>
            <br/>

            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Add Event</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default create;