import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Growl} from 'primereact/growl';

class view extends Component {

  constructor(props) {
    super(props);
    const hubId = this.props.match.params.id;
    var self =this;
    
    this.state = {
      fields: {
        hubId: hubId,
        hubNumber: "",
        hubName: "",
        hubAddress: "",
        hubPhone: "",
        hubEmail: "",
        hubAdmin: ""
      },
      errors: {}
    }

    auth.getDataById('hub',hubId).then( res => {
        if(res!==undefined) {
            self.setState({
                fields: {
                    hubId: hubId,
                    hubNumber: res.hubNumber,
                    hubName: res.hubName,
                    hubAddress: res.hubAddress,
                    hubPhone: res.hubPhone,
                    hubEmail: res.hubEmail,
                    hubAdmin: res.hubAdmin
                },
                errors: {}
              })
        }
    })

  }
  

  cancel() {
    window.location = '/#/hubs';
  }


  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> View Hub</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl"  noValidate autoComplete="off">
          <br/>
          <div className="card-header">HUB DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-4">
                <div>Hub Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Hub Name" name="hubName" id="in" value={this.state.fields.hubName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubName"]}</span>
              </div>

              <div className="p-col-4">
                <div>Hub Phone Number</div>
                <div className="p-inputgroup">
                  <InputMask disabled mask="9999999999" name="hubPhone" value={this.state.fields.hubPhone} placeholder="999-99-9999" onChange={this.updateState}></InputMask>
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubPhone"]}</span>
              </div>

              <div className="p-col-4">
                <div>Hub Email ID</div>
                <div className="p-inputgroup">
                  <InputText disabled keyfilter="email" name="hubEmail" value={this.state.fields.hubEmail} placeholder="abc@xyz.com" onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubEmail"]}</span>
              </div>

             
            </div>
            <br />
            
            <div className="p-grid p-fluid">

            <div className="p-col-12">
                <div>Hub Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <InputTextarea disabled placeholder="Hub Address" name="hubAddress" rows={2} cols={30} value={this.state.fields.hubAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubAddress"]}</span>
              </div>

             

            </div>
            <br/>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default view;