import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputText} from 'primereact/inputtext';
import {Growl} from 'primereact/growl';
import {InputTextarea} from 'primereact/inputtextarea';

class view extends Component {

  constructor(props) {
    super(props);
    const messageId = this.props.match.params.id;
    var self =this;

    this.state = {
      fields: {
        from:"",
        to:"",
        date:"",
        title:"",
        message:""
      },
      errors: {}
    }

    auth.getDataById('occupation',messageId).then( res => {
        if(res!==undefined) {
            self.setState({
                fields: {
                    messageId: messageId,
                    from:res.from,
                    to:res.to,
                    date:res.date,
                    title:res.title,
                    message:res.message
                },
                errors: {}
              })
        }
    })

  }
  
  cancel() {
    window.location = '/#/messages';
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> View Message</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" noValidate autoComplete="off">
          <br/>
          <div className="card-header">MESSAGE DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-4">
                <div>From</div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="From" name="from" id="in" value={this.state.fields.from} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["from"]}</span>
              </div>

              <div className="p-col-4">
                <div>To</div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="To" name="to" id="in" value={this.state.fields.to} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["to"]}</span>
              </div>

              <div className="p-col-4">
                <div>Date</div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Date" name="date" id="in" value={this.state.fields.date} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["date"]}</span>
              </div>
             
            </div>
            <br />

            <br/>
            <div className="p-grid p-fluid">


            <div className="p-col-4">
                <div>Title</div>
                <div className="p-inputgroup">
                <InputTextarea placeholder="Title" name="title" rows={2} cols={30} value={this.state.fields.title} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["title"]}</span>
              </div>

              <div className="p-col-4">
                <div>Message</div>
                <div className="p-inputgroup">
                <InputTextarea placeholder="Message" name="message" rows={4} cols={30} value={this.state.fields.message} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["message"]}</span>
              </div>

            </div>

          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default view;