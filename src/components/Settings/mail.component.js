import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Growl} from 'primereact/growl';

class mail extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.saveEmailSettings = this.saveEmailSettings.bind(this);
    var self = this;
    this.state = {
      fields: {
        from: "",
        userName: "",
        password: "",
        newMemberMailSubject: "",
        newMemberMailBody: "",
        forgotPasswordMailSubject: "",
        forgotPasswordMailBody: "",
      },
      errors: {}
    }
    this.created = false;

    this.otherRequiredFields = [
      "from","userName","password","newMemberMailSubject","newMemberMailBody","forgotPasswordMailSubject","forgotPasswordMailBody"
    ]

      auth.getData('email').then(res => {
          if (res.length!==0) {
              self.created = true;
              self.setState({
                  fields: {
                      emailId: res[0]._id,
                      from: res[0].from,
                      userName: res[0].userName,
                      password: res[0].password,
                      newMemberMailSubject: res[0].newMemberMailSubject,
                      newMemberMailBody: res[0].newMemberMailBody,
                      forgotPasswordMailSubject: res[0].forgotPasswordMailSubject,
                      forgotPasswordMailBody: res[0].forgotPasswordMailBody
                  },
                  errors: {}
              })
          }
      })

  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    
    for(var i=0;i<this.otherRequiredFields.length;i++) {
        if(!fields[this.otherRequiredFields[i]]){
          formIsValid = false;
          errors[this.otherRequiredFields[i]] = "Required Field";
       }
    }


   this.setState({errors: errors});
   return formIsValid;
}

showSuccess() {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: 'Settings Saved Successfully !!'});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError() {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Settings Updation Failed !!'});
}

saveEmailSettings(e) {
    var self = this;
    e.preventDefault();
    if(this.handleValidation()){
        if(!this.created) {
            auth.postData('email',this.state.fields).then(res => {
                if(res!==undefined) {
                    self.created = true;
                    if(res._id) {
                        this.showSuccess();
                        auth.getPicklistData('email');
                        window.location = '/#/emailSettings';
                    }
                } else {
                this.showError();
                }
            });
        } else {
            auth.putData('email',this.state.fields).then(res => {
                if(res!==undefined) {
                if(res._id) {
                    this.showSuccess();
                    auth.getPicklistData('email');
                    window.location = '/#/emailSettings';
                }
                } else {
                this.showError();
                }
            });
        }
   } else {
      this.showWarn();
   }
  }


  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    let errorList = Object.assign({}, this.state.errors);
    errorList[inputName] ="";
    let otherCopy = Object.assign({}, this.state.fields);
    otherCopy[inputName] = inputValue;
    this.setState({
    fields: otherCopy,errors:errorList
    });
  }

  render() {
    return (
      <div>
        {/* <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div> */}
        <br />
        <div className="card-header">EMAIL SETTINGS</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.saveEmailSettings} noValidate autoComplete="off">
          <br/>
          
            <div className="p-grid p-fluid">

            <div className="p-col-4">
                <div>From Email<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="From Email" name="from" id="in" value={this.state.fields.from} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["from"]}</span>
              </div>

              <div className="p-col-4">
                <div>Username<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Username" name="userName" id="in" value={this.state.fields.userName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["userName"]}</span>
              </div>

              <div className="p-col-4">
                <div>Password<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText type="password" placeholder="Password" name="password" id="in" value={this.state.fields.password} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["password"]}</span>
              </div>
             
            </div>
            <br />

            <div className="p-grid p-fluid">

            <div className="p-col-6">
                <div>New Member Mail Subject<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="New Member Mail Subject" name="newMemberMailSubject" rows={1} cols={30} value={this.state.fields.newMemberMailSubject} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["newMemberMailSubject"]}</span>
              </div>

              <div className="p-col-6">
                <div>Forgot Password Mail Subject<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="Forgot Password Mail Subject" name="forgotPasswordMailSubject" rows={1} cols={30} value={this.state.fields.forgotPasswordMailSubject} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["forgotPasswordMailSubject"]}</span>
              </div>
            
            </div>

            <br/>

            <div className="p-grid p-fluid">
            
            <div className="p-col-12">
                <div>New Member Mail Body<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="New Member Mail Body" name="newMemberMailBody" rows={2} cols={30} value={this.state.fields.newMemberMailBody} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["newMemberMailBody"]}</span>
              </div>
            
            </div>

            <br/>

            <div className="p-grid p-fluid">
            
            <div className="p-col-12">
                <div>Forgot Password Mail Body<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="Forgot Password Mail Body" name="forgotPasswordMailBody" rows={2} cols={30} value={this.state.fields.forgotPasswordMailBody} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["forgotPasswordMailBody"]}</span>
              </div>
            
            </div>

            <br/>

            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >SAVE</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default mail;