import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Calendar} from 'primereact/calendar';
import {Growl} from 'primereact/growl';
import {FileUpload} from 'primereact/fileupload';

const Initialfiles = [];

class create extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.addEvent = this.addEvent.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.files = this.files.bind(this);
    this.clearFiles = this.clearFiles.bind(this);
    
    this.state = {
      fields: {
        userId: "",
        eventName: "",
        eventDetails: {
            eventDate: "",
            eventTime: "",
            eventCategory: "",
        },
        eventOrganiserDetails: {
            eventOrganiserName: "",
            eventOrganiserPhone: "",
            eventOrganiserEmail: ""
        },
        eventLocationDetails: {
            eventAddress: "",
            eventMapURL: ""
        },
        eventLikes: 0,
        eventDislikes: 0,
        paths: []
      },
      errors: {},
      files: Initialfiles
    }

    this.hubs = [
      {label: 'Bangalore', value: 'Bangalore'},
      {label: 'Mangalore', value: 'Mangalore'},
    ]

    this.eventDetailsRequiredFields = [
      "eventDate","eventTime"
    ]

    this.eventOrganiserRequiredFields = [
      "eventOrganiserName"
    ]

    this.eventLocationRequiredFields = [
      "eventAddress"
    ]

    this.otherRequiredFields = [
      "eventName"
    ]

  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    for(var i=0;i<this.eventDetailsRequiredFields.length;i++) {
      if(!fields["eventDetails"][this.eventDetailsRequiredFields[i]]){
        formIsValid = false;
        let combine = "eventDetails."+this.eventDetailsRequiredFields[i];
        errors[combine] = "Required Field";
     }
    }

    for(i=0;i<this.eventOrganiserRequiredFields.length;i++) {
        if(!fields["eventOrganiserDetails"][this.eventOrganiserRequiredFields[i]]){
          formIsValid = false;
          let combine = "eventOrganiserDetails."+this.eventOrganiserRequiredFields[i];
          errors[combine] = "Required Field";
       }
    }

    for(i=0;i<this.eventLocationRequiredFields.length;i++) {
        if(!fields["eventLocationDetails"][this.eventLocationRequiredFields[i]]){
          formIsValid = false;
          let combine = "eventLocationDetails."+this.eventLocationRequiredFields[i];
          errors[combine] = "Required Field";
       }
    }

    for(i=0;i<this.otherRequiredFields.length;i++) {
      if(!fields[this.otherRequiredFields[i]]){
        formIsValid = false;
        errors[this.otherRequiredFields[i]] = "Required Field";
     }
    }

    //Email
    if(fields["eventOrganiserDetails"]["eventOrganiserEmail"] !== ""){
       let lastAtPos = fields["eventOrganiserDetails"]["eventOrganiserEmail"].lastIndexOf('@');
       let lastDotPos = fields["eventOrganiserDetails"]["eventOrganiserEmail"].lastIndexOf('.');

       if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["eventOrganiserDetails"]["eventOrganiserEmail"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["eventOrganiserDetails"]["eventOrganiserEmail"].length - lastDotPos) > 2)) {
          formIsValid = false;
          errors["eventOrganiserDetails"]["eventOrganiserEmail"] = "Email is not valid";
        }
   }  

   if(this.state.files.length===0) {
     formIsValid = false;
     errors["fileError"] = "Please Upload Images";
   }



   this.setState({errors: errors});
   return formIsValid;
}

showSuccess() {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: 'Event Created Successfully !!'});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError() {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Event Creation Failed !!'});
}

addEvent(e) {
    e.preventDefault();
    if(this.handleValidation()){
      this.uploadFiles();
   } else {
      this.showWarn();
   }
  }

  cancel() {
    window.location = '/#/events';
  }

  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    if(e.target.name==='eventDetails.eventDate') {
      var date = e.target.value;
      try {
        inputValue = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
      } catch (ex) {
        inputValue = "";
      }
    } else if(e.target.name==='eventDetails.eventTime') {
      var time = e.target.value;
      inputValue = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    }
    let errorList = Object.assign({}, this.state.errors);
    if(inputName.indexOf('.')>-1) {
      var object = inputName.split('.')[0];
      var field = inputName.split('.')[1];
      if(object==='eventDetails') {
        let eventDetailsCopy = Object.assign({}, this.state.fields);
        eventDetailsCopy.eventDetails[field] = inputValue;
        let combine = "eventDetails."+field;
        errorList[combine] ="";
        this.setState({fields:eventDetailsCopy,errors:errorList});
       // this.setState({errors:errorList});
      } else if(object==='eventOrganiserDetails') {
        let eventOrganiserDetailsCopy = Object.assign({}, this.state.fields);
        eventOrganiserDetailsCopy.eventOrganiserDetails[field] = inputValue;
        let combine = "eventOrganiserDetails."+field;
        errorList[combine] ="";
        this.setState({fields:eventOrganiserDetailsCopy,errors:errorList});
      } else if(object==='eventLocationDetails') {
        let eventLocationDetailsCopy = Object.assign({}, this.state.fields);
        eventLocationDetailsCopy.eventLocationDetails[field] = inputValue;
        let combine = "eventLocationDetails."+field;
        errorList[combine] ="";
        this.setState({fields:eventLocationDetailsCopy,errors:errorList});
      }
    } else {
      errorList[inputName] ="";
      let otherCopy = Object.assign({}, this.state.fields);
      otherCopy[inputName] = inputValue;
      this.setState({
        fields: otherCopy,errors:errorList
      });
    }
  }

  files(e) {
    let errorList = Object.assign({}, this.state.errors);
    errorList['fileError'] = "";
    this.setState({ files: [...Initialfiles, ...e.originalEvent.target.files],errors: errorList});
  }

  uploadFiles() {
    var self = this;
    var data = new FormData();
    data.append('folderName','Events');
    data.append('name', this.state.fields.eventName);
    for (const file of this.state.files) {
      data.append('files[]', file, file.name);
    }
    auth.postImage('uploadFiles',data).then(res => {
      let fieldCopy = Object.assign({}, this.state.fields);
      if(res.path!==[]) {
        fieldCopy['paths'] = res.path;
        self.setState({fields:fieldCopy})
          auth.postData('event',self.state.fields).then(res => {
            if(res!==undefined) {
              if(res._id) {
                this.showSuccess();
                setTimeout(
                  function() {
                    window.location = '/#/events';
                  },
                  1000
              );
              }
            } else {
              this.showError();
            }
        });
      } else {
        this.showError();
      }
  });


    //console.log(this.state.files);
  }

  clearFiles() {
    this.setState({files:[]});
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> Add Event</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.addEvent} noValidate autoComplete="off">
          <br/>
          <div className="card-header">EVENT DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-3">
                <div>Event Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Event Name" name="eventName" id="in" value={this.state.fields.eventName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventName"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Date<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar keyfilter="num" placeholder="Event Date" name="eventDetails.eventDate" value={this.state.fields.eventDetails.eventDate} onChange={this.updateState} monthNavigator={true} yearNavigator={true} yearRange="1900:2100" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventDate"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Time<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar placeholder="Event Time" name="eventDetails.eventTime" value={this.state.fields.eventDetails.eventTime} onChange={this.updateState} timeOnly={true} hourFormat="12" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventTime"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Category<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Event Category" name="eventDetails.eventCategory" id="in" value={this.state.fields.eventDetails.eventCategory} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventCategory"]}</span>
              </div>

            </div>
            <br />
            <div className="card-header">EVENT ORGANISER DETAILS</div>
            <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Name" name="eventOrganiserDetails.eventOrganiserName" id="in" value={this.state.fields.eventOrganiserDetails.eventOrganiserName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserName"]}</span>
              </div>

              <div className="p-col-4">
                <div>Phone Number<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputMask mask="9999999999" name="eventOrganiserDetails.eventOrganiserPhone" value={this.state.fields.eventOrganiserDetails.eventOrganiserPhone} placeholder="999-99-9999" onChange={this.updateState}></InputMask>
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserPhone"]}</span>
              </div>

              <div className="p-col-4">
                <div>Email ID<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText keyfilter="email" name="eventOrganiserDetails.eventOrganiserEmail" value={this.state.fields.eventOrganiserDetails.eventOrganiserEmail} placeholder="abc@xyz.com" onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserEmail"]}</span>
              </div>

            </div>
            <br />

            <br />
            <div className="card-header">EVENT LOCATION DETAILS</div>
            <br/>

            <div className="p-grid p-fluid">

            <div className="p-col-6">
                <div>Event Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <InputTextarea placeholder="Event Address" name="eventLocationDetails.eventAddress" rows={1} cols={30} value={this.state.fields.eventLocationDetails.eventAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventLocationDetails.eventAddress"]}</span>
              </div>

              <div className="p-col-6">
                <div>Event Map URL<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Event Name" name="eventLocationDetails.eventMapURL" id="in" value={this.state.fields.eventLocationDetails.eventMapURL} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventLocationDetails.eventMapURL"]}</span>
              </div>

            </div>

            <br />
            
            <div className="card-header">EVENT IMAGES</div>
            <br/>

            <div className="p-grid p-fluid">

            <div className="p-col-12">
                <div>Upload Images<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <FileUpload name="files[]" url="https://www.primefaces.org/primereact/upload.php"  onSelect={this.files} onClear={this.clearFiles}
                                multiple={true} accept="image/*" maxFileSize={1000000} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["fileError"]}</span>
              </div>

            </div>

            <br/>
            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Add Event</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default create;