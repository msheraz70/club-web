import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Calendar} from 'primereact/calendar';
import {Growl} from 'primereact/growl';
import config from '../../config';

class view extends Component {

  constructor(props) {
    super(props);
    const eventId = this.props.match.params.id;
    var self =this;
    
    this.state = {
      fields: {
        eventId: eventId,
        userId: "",
        eventName: "",
        eventDetails: {
            eventDate: "",
            eventTime: "",
            eventCategory: "",
        },
        eventOrganiserDetails: {
            eventOrganiserName: "",
            eventOrganiserPhone: "",
            eventOrganiserEmail: ""
        },
        eventLocationDetails: {
            eventAddress: "",
            eventMapURL: ""
        },
        eventLikes: 0,
        eventDislikes: 0,
        paths: []
      },
      errors: {}
    }

    auth.getDataById('event',eventId).then( res => {
        if(res!==undefined) {
            self.setState({
                fields: {
                    eventId: eventId,
                    userId: "",
                    eventName: res.eventName,
                    eventDetails: {
                        eventDate: res.eventDetails.eventDate,
                        eventTime: res.eventDetails.eventTime,
                        eventCategory: res.eventDetails.eventCategory,
                    },
                    eventOrganiserDetails: {
                        eventOrganiserName: res.eventOrganiserDetails.eventOrganiserName,
                        eventOrganiserPhone: res.eventOrganiserDetails.eventOrganiserPhone,
                        eventOrganiserEmail: res.eventOrganiserDetails.eventOrganiserEmail
                    },
                    eventLocationDetails: {
                        eventAddress: res.eventLocationDetails.eventAddress,
                        eventMapURL: res.eventLocationDetails.eventMapURL
                    },
                    eventLikes: res.eventLikes,
                    eventDislikes: res.eventDislikes,
                    paths: res.paths
                },
                errors: {}
              })
        }
    })

  }

  cancel() {
    window.location = '/#/events';
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> View Event</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl"  noValidate autoComplete="off">
          <br/>
          <div className="card-header">EVENT DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-3">
                <div>Event Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Event Name" name="eventName" id="in" value={this.state.fields.eventName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventName"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Date<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar disabled placeholder="Event Date" name="eventDetails.eventDate" value={this.state.fields.eventDetails.eventDate} onChange={this.updateState} monthNavigator={true} yearNavigator={true} yearRange="1900:2100" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventDate"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Time<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar disabled placeholder="Event Time" name="eventDetails.eventTime" value={this.state.fields.eventDetails.eventTime} onChange={this.updateState} timeOnly={true} hourFormat="12" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventTime"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Category<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Event Category" name="eventDetails.eventCategory" id="in" value={this.state.fields.eventDetails.eventCategory} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventCategory"]}</span>
              </div>

            </div>
            <br />
            <div className="card-header">EVENT ORGANISER DETAILS</div>
            <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Name" name="eventOrganiserDetails.eventOrganiserName" id="in" value={this.state.fields.eventOrganiserDetails.eventOrganiserName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserName"]}</span>
              </div>

              <div className="p-col-4">
                <div>Phone Number<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputMask disabled mask="9999999999" name="eventOrganiserDetails.eventOrganiserPhone" value={this.state.fields.eventOrganiserDetails.eventOrganiserPhone} placeholder="999-99-9999" onChange={this.updateState}></InputMask>
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserPhone"]}</span>
              </div>

              <div className="p-col-4">
                <div>Email ID<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled keyfilter="email" name="eventOrganiserDetails.eventOrganiserEmail" value={this.state.fields.eventOrganiserDetails.eventOrganiserEmail} placeholder="abc@xyz.com" onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserEmail"]}</span>
              </div>

            </div>
            <br />

            <br />
            <div className="card-header">EVENT LOCATION DETAILS</div>
            <br/>

            <div className="p-grid p-fluid">

            <div className="p-col-6">
                <div>Event Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <InputTextarea disabled placeholder="Event Address" name="eventLocationDetails.eventAddress" rows={1} cols={30} value={this.state.fields.eventLocationDetails.eventAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventLocationDetails.eventAddress"]}</span>
              </div>

              <div className="p-col-6">
                <div>Event Map URL<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Event Name" name="eventLocationDetails.eventMapURL" id="in" value={this.state.fields.eventLocationDetails.eventMapURL} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventLocationDetails.eventMapURL"]}</span>
              </div>

            </div>
            <br />

            <div className={this.state.fields.paths.length===0 ? 'hidden' : ''}>
              Uploaded Images
              <br/>
            {this.state.fields.paths.map((i, index) =>
               <div key={index}>
               <div className={i===null ? 'hidden' : ''}><img src={config.Url+i} alt={i} style={{width:'100px',height:'100px'}}/></div>
                <br/>
              </div>)}
            </div>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default view;