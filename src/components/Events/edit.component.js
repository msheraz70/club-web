import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {Calendar} from 'primereact/calendar';
import {Growl} from 'primereact/growl';
import {FileUpload} from 'primereact/fileupload';
import config from '../../config';

const Initialfiles = [];

class edit extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.editEvent = this.editEvent.bind(this);
    const eventId = this.props.match.params.id;
    this.uploadFiles = this.uploadFiles.bind(this);
    this.files = this.files.bind(this);
    this.clearFiles = this.clearFiles.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
    var self =this;
    
    this.state = {
      fields: {
        eventId: eventId,
        userId: "",
        eventName: "",
        eventDetails: {
            eventDate: "",
            eventTime: "",
            eventCategory: "",
        },
        eventOrganiserDetails: {
            eventOrganiserName: "",
            eventOrganiserPhone: "",
            eventOrganiserEmail: ""
        },
        eventLocationDetails: {
            eventAddress: "",
            eventMapURL: ""
        },
        eventLikes: 0,
        eventDislikes: 0,
        paths: []
      },
      errors: {},
      files: Initialfiles
    }

    this.hubs = [
      {label: 'Bangalore', value: 'Bangalore'},
      {label: 'Mangalore', value: 'Mangalore'},
    ]

    this.eventDetailsRequiredFields = [
      "eventDate","eventTime"
    ]

    this.eventOrganiserRequiredFields = [
      "eventOrganiserName"
    ]

    this.eventLocationRequiredFields = [
      "eventAddress"
    ]

    this.otherRequiredFields = [
      "eventName"
    ]

    auth.getDataById('event',eventId).then( res => {
        if(res!==undefined) {
            self.setState({
                fields: {
                    eventId: eventId,
                    userId: "",
                    eventName: res.eventName,
                    eventDetails: {
                        eventDate: res.eventDetails.eventDate,
                        eventTime: res.eventDetails.eventTime,
                        eventCategory: res.eventDetails.eventCategory,
                    },
                    eventOrganiserDetails: {
                        eventOrganiserName: res.eventOrganiserDetails.eventOrganiserName,
                        eventOrganiserPhone: res.eventOrganiserDetails.eventOrganiserPhone,
                        eventOrganiserEmail: res.eventOrganiserDetails.eventOrganiserEmail
                    },
                    eventLocationDetails: {
                        eventAddress: res.eventLocationDetails.eventAddress,
                        eventMapURL: res.eventLocationDetails.eventMapURL
                    },
                    eventLikes: res.eventLikes,
                    eventDislikes: res.eventDislikes,
                    paths: res.paths
                },
                errors: {},
                files: Initialfiles
              })
        }
    })

  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    for(var i=0;i<this.eventDetailsRequiredFields.length;i++) {
      if(!fields["eventDetails"][this.eventDetailsRequiredFields[i]]){
        formIsValid = false;
        let combine = "eventDetails."+this.eventDetailsRequiredFields[i];
        errors[combine] = "Required Field";
     }
    }

    for(i=0;i<this.eventOrganiserRequiredFields.length;i++) {
        if(!fields["eventOrganiserDetails"][this.eventOrganiserRequiredFields[i]]){
          formIsValid = false;
          let combine = "eventOrganiserDetails."+this.eventOrganiserRequiredFields[i];
          errors[combine] = "Required Field";
       }
    }

    for(i=0;i<this.eventLocationRequiredFields.length;i++) {
        if(!fields["eventLocationDetails"][this.eventLocationRequiredFields[i]]){
          formIsValid = false;
          let combine = "eventLocationDetails."+this.eventLocationRequiredFields[i];
          errors[combine] = "Required Field";
       }
    }

    for(i=0;i<this.otherRequiredFields.length;i++) {
      if(!fields[this.otherRequiredFields[i]]){
        formIsValid = false;
        errors[this.otherRequiredFields[i]] = "Required Field";
     }
    }

    //Email
    if(fields["eventOrganiserDetails"]["eventOrganiserEmail"] !== ""){
       let lastAtPos = fields["eventOrganiserDetails"]["eventOrganiserEmail"].lastIndexOf('@');
       let lastDotPos = fields["eventOrganiserDetails"]["eventOrganiserEmail"].lastIndexOf('.');

       if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["eventOrganiserDetails"]["eventOrganiserEmail"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["eventOrganiserDetails"]["eventOrganiserEmail"].length - lastDotPos) > 2)) {
          formIsValid = false;
          errors["eventOrganiserDetails"]["eventOrganiserEmail"] = "Email is not valid";
        }
   }  

   if(this.state.files.length===0 && this.state.fields.paths.length===0) {
    formIsValid = false;
    errors["fileError"] = "Please Upload Images";
  }

   this.setState({errors: errors});
   return formIsValid;
}

showSuccess(msg) {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: msg});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError() {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Event Updation Failed !!'});
}

editEvent(e) {
    e.preventDefault();
    if(this.handleValidation()){
      this.uploadFiles();
   } else {
      this.showWarn();
   }
  }

  uploadFiles() {
    if(this.state.files.length!==0) {
      var self = this;
      var data = new FormData();
      data.append('folderName','Events');
      data.append('name', this.state.fields.eventName);
      for (const file of this.state.files) {
        data.append('files[]', file, file.name);
      }
      auth.postImage('uploadFiles',data).then(res => {
        let fieldCopy = Object.assign({}, this.state.fields);
        if(res.path!==[]) {
          for(var i=0;i<res.path.length;i++) {
            fieldCopy['paths'].push(res.path[i]);
          }
          self.setState({fields:fieldCopy})
          auth.putData('event',this.state.fields).then(res => {
            if(res!==undefined) {
              if(res._id) {
                this.showSuccess('Event Edited Successfully !!');
                setTimeout(
                  function() {
                    window.location = '/#/events';
                  },
                  1000
              );
              }
            } else {
              this.showError();
            }
        });
        } else {
          this.showError();
        }
    });
  } else {
    auth.putData('event',this.state.fields).then(res => {
      if(res!==undefined) {
        if(res._id) {
          this.showSuccess('Event Edited Successfully !!');
          setTimeout(
            function() {
              window.location = '/#/events';
            },
            1000
        );
        }
      } else {
        this.showError();
      }
  });
 } 
}

  files(e) {
    let errorList = Object.assign({}, this.state.errors);
    errorList['fileError'] = "";
    this.setState({ files: [...Initialfiles, ...e.originalEvent.target.files],errors: errorList});
  }

  clearFiles() {
    this.setState({files:[]});
  }

  cancel() {
    window.location = '/#/events';
  }

  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    let errorList = Object.assign({}, this.state.errors);
    if(inputName.indexOf('.')>-1) {
      var object = inputName.split('.')[0];
      var field = inputName.split('.')[1];
      if(object==='eventDetails') {
        let eventDetailsCopy = Object.assign({}, this.state.fields);
        eventDetailsCopy.eventDetails[field] = inputValue;
        let combine = "eventDetails."+field;
        errorList[combine] ="";
        this.setState({fields:eventDetailsCopy,errors:errorList});
       // this.setState({errors:errorList});
      } else if(object==='eventOrganiserDetails') {
        let eventOrganiserDetailsCopy = Object.assign({}, this.state.fields);
        eventOrganiserDetailsCopy.eventOrganiserDetails[field] = inputValue;
        let combine = "eventOrganiserDetails."+field;
        errorList[combine] ="";
        this.setState({fields:eventOrganiserDetailsCopy,errors:errorList});
      } else if(object==='eventLocationDetails') {
        let eventLocationDetailsCopy = Object.assign({}, this.state.fields);
        eventLocationDetailsCopy.eventLocationDetails[field] = inputValue;
        let combine = "eventLocationDetails."+field;
        errorList[combine] ="";
        this.setState({fields:eventLocationDetailsCopy,errors:errorList});
      }
    } else {
      errorList[inputName] ="";
      let otherCopy = Object.assign({}, this.state.fields);
      otherCopy[inputName] = inputValue;
      this.setState({
        fields: otherCopy,errors:errorList
      });
    }
  }

  deleteImage(imgPath) {
    var trueCopy = this.state.fields;
    var data = {
      "imagePath": imgPath
    };
    auth.deleteImage('uploadFiles',data).then(res => {
      if(res!==undefined) {
        var index = trueCopy.paths.indexOf(imgPath);
        trueCopy.paths.splice(index, 1);
        this.setState({
          fields: trueCopy
        })
        this.showSuccess('Image Deleted Successfully !');
      }
    });
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> Edit Event</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.editEvent} noValidate autoComplete="off">
          <br/>
          <div className="card-header">EVENT DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-3">
                <div>Event Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Event Name" name="eventName" id="in" value={this.state.fields.eventName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventName"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Date<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar placeholder="Event Date" name="eventDetails.eventDate" value={this.state.fields.eventDetails.eventDate} onChange={this.updateState} monthNavigator={true} yearNavigator={true} yearRange="1900:2100" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventDate"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Time<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar placeholder="Event Time" name="eventDetails.eventTime" value={this.state.fields.eventDetails.eventTime} onChange={this.updateState} timeOnly={true} hourFormat="12" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventTime"]}</span>
              </div>

              <div className="p-col-3">
                <div>Event Category<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Event Category" name="eventDetails.eventCategory" id="in" value={this.state.fields.eventDetails.eventCategory} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventDetails.eventCategory"]}</span>
              </div>

            </div>
            <br />
            <div className="card-header">EVENT ORGANISER DETAILS</div>
            <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Name" name="eventOrganiserDetails.eventOrganiserName" id="in" value={this.state.fields.eventOrganiserDetails.eventOrganiserName} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserName"]}</span>
              </div>

              <div className="p-col-4">
                <div>Phone Number<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputMask mask="9999999999" name="eventOrganiserDetails.eventOrganiserPhone" value={this.state.fields.eventOrganiserDetails.eventOrganiserPhone} placeholder="999-99-9999" onChange={this.updateState}></InputMask>
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserPhone"]}</span>
              </div>

              <div className="p-col-4">
                <div>Email ID<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText keyfilter="email" name="eventOrganiserDetails.eventOrganiserEmail" value={this.state.fields.eventOrganiserDetails.eventOrganiserEmail} placeholder="abc@xyz.com" onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventOrganiserDetails.eventOrganiserEmail"]}</span>
              </div>

            </div>
            <br />

            <br />
            <div className="card-header">EVENT LOCATION DETAILS</div>
            <br/>

            <div className="p-grid p-fluid">

            <div className="p-col-6">
                <div>Event Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <InputTextarea placeholder="Event Address" name="eventLocationDetails.eventAddress" rows={1} cols={30} value={this.state.fields.eventLocationDetails.eventAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventLocationDetails.eventAddress"]}</span>
              </div>

              <div className="p-col-6">
                <div>Event Map URL<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Event Name" name="eventLocationDetails.eventMapURL" id="in" value={this.state.fields.eventLocationDetails.eventMapURL} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["eventLocationDetails.eventMapURL"]}</span>
              </div>

            </div>
            <br />

            <div className="card-header">EVENT IMAGES</div>
            <br/>

            <div className="p-grid p-fluid">

            <div className="p-col-12">
                <div>Upload Images<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                <FileUpload name="files[]" url="https://www.primefaces.org/primereact/upload.php"  onSelect={this.files} onClear={this.clearFiles}
                                multiple={true} accept="image/*" maxFileSize={1000000} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["fileError"]}</span>
              </div>

            </div>

            <br/>
            <div className={this.state.fields.paths.length===0 ? 'hidden' : ''}>
              Uploaded Images
              <br/>
            {this.state.fields.paths.map((i, index) =>
               <div key={index}>
               <div className={i===null ? 'hidden' : ''}><img src={config.Url+i} alt={i} style={{width:'100px',height:'100px'}}/><Button type="button" variant="contained" style={{float:'right'}} onClick={() => this.deleteImage(i)} >
        Delete
       
      </Button></div>
                <br/>
              </div>)}
            </div>

            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Edit Event</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default edit;