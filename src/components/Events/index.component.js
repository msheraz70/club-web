import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import ChildMessageRenderer from "../../common/ChildMessageRenderer";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Growl} from 'primereact/growl';
class User extends Component {
  constructor(props) {
    super(props);
    this.onGridReady = this.onGridReady.bind(this);
    this.onPageSizeChanged = this.onPageSizeChanged.bind(this);
    this.state = {
        columnDefs: [{
          headerName: "Event Name", field: "eventName", filter: "agTextColumnFilter", filterParams: { clearButton: true }
        }, {
          headerName: "Event Date", field: "eventDetails.eventDate", filter: "agTextColumnFilter", filterParams: { clearButton: true }
        }, {
          headerName: "Event Time", field: "eventDetails.eventTime", filter: "agTextColumnFilter", filterParams: { clearButton: true }
        }, {
          headerName: "Event Organiser Name", field: "eventOrganiserDetails.eventOrganiserName", filter: "agTextColumnFilter", filterParams: { clearButton: true }
        }, {
          headerName: "Event Address", field: "eventLocationDetails.eventAddress", filter: "agTextColumnFilter", filterParams: { clearButton: true }
        },
        //  {
        //   headerName: "Event Likes", field: "eventLikes", filter: "agTextColumnFilter", filterParams: { clearButton: true }
        // }, 
        {
          headerName: "Actions", cellRenderer: "childMessageRenderer"
        }],
        rowData: [],
        context: { componentParent: this },
        frameworkComponents: {
            childMessageRenderer: ChildMessageRenderer
          },
        open : false,
        setOpen : false,
        id : null,
        userId : null
      }
  }
  
  onGridReady = params => {
    this.gridApi = params.api;
    auth.getData('event').then(res => {
        params.api.setRowData(res);
        this.setState({rowData:res});
    });
  };

  showSuccess() {
    this.growl.show({severity: 'success', summary: 'Success Message', detail: 'Event Deleted Successfully !!'});
  }
  
  showInfo() {
    this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
  }
  
  showWarn() {
    this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
  }
  
  showError() {
    this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Event Creation Failed !!'});
  }

  add() {
      window.location = '/#/events/create';
  }

  view(param) {
    window.location = '/#/events/view/'+param.data._id;
  }

  edit(param) {
    window.location = '/#/events/edit/'+param.data._id;
  }

  deleteImage(imgPath) {
    var data = {
      "imagePath": imgPath,
      "deleteFolder": true
    };
    auth.deleteImage('uploadFiles',data).then(res => {
      if(res!==undefined) {
       console.log('Images Deleted');
      }
    });
  }

  delete(param) {
    //console.log('delete'+param.id);
    this.setState({setOpen : true, eventId : param.data._id, eventName : param.data.eventName, id:param.id});
  }   

  handleClose(choice) {
    var self = this;
    //var selectedData = this.gridApi.getSelectedRows();
    //var params = { force: true };
    if(choice) {
        auth.deleteData('event',this.state.eventId).then(res => {
            self.state.rowData.splice(this.state.id,1);
            self.gridApi.setRowData(self.state.rowData);
            self.deleteImage('public/images/uploads/Events/'+ this.state.eventName);
            this.showSuccess();
            //self.gridApi.refreshCells(params)
            //self.gridApi.updateRowData({ remove: selectedData })
            //console.log('deleted successfully')
        })
    } else {
        this.setState({id : null});
    }
    this.setState({setOpen : false});
  }

  onPageSizeChanged(newPageSize) {
    var value = document.getElementById("page-size").value;
    this.gridApi.paginationSetPageSize(Number(value));
  }

  render() {
    return (
        <div>
        <div >
            <Button variant="contained" onClick={this.add} color="secondary" >Add Event</Button>
            </div>
            <br/>
            <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
            <div className="card-header">List Events</div>
      <div 
        className="ag-theme-material"
        style={{ 
        height: '350px', 
        width: '100%' }} 
      >
      
        <AgGridReact
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData} onGridReady={this.onGridReady} frameworkComponents={this.state.frameworkComponents} context={this.state.context} pagination={true}>
        </AgGridReact>
        <div className="test-header">
            Page Size:
            <select onChange={this.onPageSizeChanged} id="page-size">
              <option value="10" selected="">
                10
              </option>
              <option value="100">100</option>
              <option value="500">500</option>
              <option value="1000">1000</option>
            </select>
          </div>
      </div>
      <Dialog
        open={this.state.setOpen}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete Event</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete this Event ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={(e) => this.handleClose(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={(e) => this.handleClose(true)} color="primary" autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
      </div>
    );
  }
}

export default User;