import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { InputTextarea } from 'primereact/inputtextarea';
import config from '../../config';

const Initialfiles = [];

class view extends Component {

    constructor(props) {
        super(props);
        const facilityId = this.props.match.params.id;
        var self = this;

        this.state = {
            fields: {
                facilityId: facilityId,
                facilityName: "",
                facilityDescription: "",
                facilityLocation: "",
                facilityTimings: "",
                facilityImages: []
            },
            errors: {},
            files: Initialfiles
        }


        auth.getDataById('facility',facilityId).then( res => {
            if(res!==undefined) {
                self.setState({
                    fields: {
                        facilityId: facilityId,
                        facilityName: res.facilityName,
                        facilityDescription: res.facilityDescription,
                        facilityLocation: res.facilityLocation,
                        facilityTimings: res.facilityTimings,
                        facilityImages: res.facilityImages
                    },
                    errors: {},
                    files: Initialfiles
                  })
            }
        })
    
      }

    cancel() {
        window.location = '/#/facilities';
    }

    render() {
        return (
            <div>
                <div >
                    <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
                </div>
                <br />
                <div className="card-header"> View Facility</div>
                <Growl ref={(el) => this.growl = el} position="topright" style={{ top: '75px', right: '5px' }} />
                <Container maxWidth="lg" className="container">
                    <form name="formEl" noValidate autoComplete="off">
                        <br />
                        <div className="card-header">FACILITY DETAILS</div>
                        <br />
                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Facility Name<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputText disabled placeholder="Facility Name" name="facilityName" id="facilityName" value={this.state.fields.facilityName} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityName"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Facility Description<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputTextarea disabled placeholder="Facility Description" name="facilityDescription" rows={1} cols={30} value={this.state.fields.facilityDescription} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityDescription"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Facility Location</div>
                                <div className="p-inputgroup">
                                    <InputText disabled placeholder="Facility Location" name="facilityLocation" id="facilityLocation" value={this.state.fields.facilityLocation} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityLocation"]}</span>
                            </div>

                            <div className="p-col-4">
                                <div>Facility Timings</div>
                                <div className="p-inputgroup">
                                    <InputText disabled placeholder="Facility Timings" name="facilityTimings" id="facilityTimings" value={this.state.fields.facilityTimings} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityTimings"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="card-header">FACILITY IMAGES</div>
                        <br />

                        <div className={this.state.fields.facilityImages.length===0 ? 'hidden' : ''}>
              Uploaded Images
              <br/>
            {this.state.fields.facilityImages.map((i, index) =>
               <div key={index}>
               <div className={i===null ? 'hidden' : ''}><img src={config.Url+i} alt={i} style={{width:'100px',height:'100px'}}/></div>
                <br/>
              </div>)}
            </div>
                    </form>
                    <br />
                    <br />

                </Container>
            </div>

        );
    }
}

export default view;