import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { InputTextarea } from 'primereact/inputtextarea';
import { FileUpload } from 'primereact/fileupload';
import config from '../../config';

const Initialfiles = [];

class edit extends Component {

    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.editFacility = this.editFacility.bind(this);
        this.files = this.files.bind(this);
        const facilityId = this.props.match.params.id;
        var self = this;

        this.state = {
            fields: {
                facilityId: facilityId,
                facilityName: "",
                facilityDescription: "",
                facilityLocation: "",
                facilityTimings: "",
                facilityImages: []
            },
            errors: {},
            files: Initialfiles
        }

        this.otherRequiredFields = [
            "facilityName", "facilityDescription"
        ]

        auth.getDataById('facility',facilityId).then( res => {
            if(res!==undefined) {
                self.setState({
                    fields: {
                        facilityId: facilityId,
                        facilityName: res.facilityName,
                        facilityDescription: res.facilityDescription,
                        facilityLocation: res.facilityLocation,
                        facilityTimings: res.facilityTimings,
                        facilityImages: res.facilityImages
                    },
                    errors: {},
                    files: Initialfiles
                  })
            }
        })
    
      }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        for (var i = 0; i < this.otherRequiredFields.length; i++) {
            if (!fields[this.otherRequiredFields[i]]) {
                formIsValid = false;
                errors[this.otherRequiredFields[i]] = "Required Field";
            }
        }

        if (this.state.files.length === 0 && this.state.fields.facilityImages.length === 0) {
            formIsValid = false;
            errors["fileError"] = "Please Upload Images";
        }


        this.setState({ errors: errors });
        return formIsValid;
    }

    showSuccess(msg) {
        this.growl.show({ severity: 'success', summary: 'Success Message', detail: msg });
    }

    showInfo() {
        this.growl.show({ severity: 'info', summary: 'Info Message', detail: 'Validation Failed' });
    }

    showWarn() {
        this.growl.show({ severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!' });
    }

    showError() {
        this.growl.show({ severity: 'error', summary: 'Error Message', detail: 'Facility Updation Failed !!' });
    }

    uploadImages() {
        if(this.state.files.length!==0) {
            var self = this;
            var data = new FormData();
            data.append('folderName', 'Facility');
            data.append('name', this.state.fields.facilityName);
            for (const file of this.state.files) {
                data.append('files[]', file, file.name);
            }
            auth.postImage('uploadFiles', data).then(res => {
                let fieldCopy = Object.assign({}, this.state.fields);
                if (res.path !== []) {
                    for(var i=0;i<res.path.length;i++) {
                        fieldCopy['facilityImages'].push(res.path[i]);
                    }
                    self.setState({ fields: fieldCopy })
                    auth.putData('facility', this.state.fields).then(res => {
                        if (res !== undefined) {
                            if (res._id) {
                                this.showSuccess('Facility Edited Successfully !!');
                                setTimeout(
                                    function () {
                                        window.location = '/#/facilities';
                                    },
                                    1000
                                );
                            }
                        } else {
                            this.showError();
                        }
                    });
                } else {
                    this.showError();
                }
            });
        } else {
            auth.putData('facility', this.state.fields).then(res => {
                if (res !== undefined) {
                    if (res._id) {
                        this.showSuccess('Facility Edited Successfully !!');
                        setTimeout(
                            function () {
                                window.location = '/#/facilities';
                            },
                            1000
                        );
                    }
                } else {
                    this.showError();
                }
            });
        }
    }

    editFacility(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            this.uploadImages();
        } else {
            this.showWarn();
        }
    }

    cancel() {
        window.location = '/#/facilities';
    }

    updateState(e) {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        let errorList = Object.assign({}, this.state.errors);
        errorList[inputName] = "";
        let otherCopy = Object.assign({}, this.state.fields);
        otherCopy[inputName] = inputValue;
        this.setState({
            fields: otherCopy, errors: errorList
        });
    }

    files(e) {
        let errorList = Object.assign({}, this.state.errors);
        errorList['fileError'] = "";
        this.setState({ files: [...Initialfiles, ...e.originalEvent.target.files], errors: errorList });
    }

    clearFiles() {
        this.setState({ files: [] });
    }

    deleteImage(imgPath) {
        var trueCopy = this.state.fields;
        var data = {
          "imagePath": imgPath
        };
        auth.deleteImage('uploadFiles',data).then(res => {
          if(res!==undefined) {
            var index = trueCopy.facilityImages.indexOf(imgPath);
            trueCopy.facilityImages.splice(index, 1);
            this.setState({
              fields: trueCopy
            })
            this.showSuccess('Image Deleted Successfully !');
          }
        });
      }

    render() {
        return (
            <div>
                <div >
                    <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
                </div>
                <br />
                <div className="card-header"> Edit Facility</div>
                <Growl ref={(el) => this.growl = el} position="topright" style={{ top: '75px', right: '5px' }} />
                <Container maxWidth="lg" className="container">
                    <form name="formEl" onSubmit={this.editFacility} noValidate autoComplete="off">
                        <br />
                        <div className="card-header">FACILITY DETAILS</div>
                        <br />
                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Facility Name<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputText disabled placeholder="Facility Name" name="facilityName" id="facilityName" value={this.state.fields.facilityName} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityName"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Facility Description<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputTextarea placeholder="Facility Description" name="facilityDescription" rows={2} cols={30} value={this.state.fields.facilityDescription} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityDescription"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Facility Location</div>
                                <div className="p-inputgroup">
                                    <InputText placeholder="Facility Location" name="facilityLocation" id="facilityLocation" value={this.state.fields.facilityLocation} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityLocation"]}</span>
                            </div>

                            <div className="p-col-4">
                                <div>Facility Timings</div>
                                <div className="p-inputgroup">
                                    <InputText placeholder="Facility Timings" name="facilityTimings" id="facilityTimings" value={this.state.fields.facilityTimings} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["facilityTimings"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="card-header">FACILITY IMAGES</div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Upload Images<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <FileUpload name="files[]" url="https://www.primefaces.org/primereact/upload.php" onSelect={this.files} onClear={this.clearFiles}
                                        multiple={true} accept="image/*" maxFileSize={1000000} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["fileError"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className={this.state.fields.facilityImages.length===0 ? 'hidden' : ''}>
              Uploaded Images
              <br/>
            {this.state.fields.facilityImages.map((i, index) =>
               <div key={index}>
               <div className={i===null ? 'hidden' : ''}><img src={config.Url+i} alt={i} style={{width:'100px',height:'100px'}}/><Button type="button" variant="contained" style={{float:'right'}} onClick={() => this.deleteImage(i)} >
        Delete
       
      </Button></div>
                <br/>
              </div>)}
            </div>

                        <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Edit Facility</Button>
                    </form>
                    <br />
                    <br />

                </Container>
            </div>

        );
    }
}

export default edit;