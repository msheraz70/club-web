import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputText} from 'primereact/inputtext';
import {Growl} from 'primereact/growl';

class Profile extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.changePassword = this.changePassword.bind(this);
    const userId = auth.getId();
    
    this.state = {
      fields: {
        id: userId,
        password: ""
      },
      repassword: "",
      errors: {},
      response: {}
    }

  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

      if(!fields['password']){
        formIsValid = false;
        errors['password'] = "Required Field";
      }

      if(!this.state['repassword']){
        formIsValid = false;
        errors['repassword'] = "Required Field";
      }

      if(fields['password']!=="" && this.state['repassword']!=="" && fields['password']!==this.state['repassword']){
        formIsValid = false;
        errors['noMatch'] = "Password do not match !";
      }

   this.setState({errors: errors});
   return formIsValid;
}

showSuccess(msg) {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: msg});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError(msg) {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: msg});
}



changePassword(e) {
    e.preventDefault();
    if(this.handleValidation()){
      auth.putData('users',this.state.fields).then(res => {
        if(res!==undefined) {
          if(res._id) {
            this.showSuccess('Password Changed Successfully !!');
            setTimeout(
                function() {
                    window.location = '/#/users';
                },
                1000
            );
          }
        } else {
          this.showError('Password Changed Successfully !!');
        }
    });
   } else {
      this.showWarn();
   }
  }


  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    let errorList = Object.assign({}, this.state.errors);
    errorList[inputName] ="";
    if(inputName==='repassword') {
        this.setState({repassword:inputValue,errors:errorList});
    } else {
        let otherCopy = Object.assign({}, this.state.fields);
        otherCopy[inputName] = inputValue;
        this.setState({
        fields: otherCopy,errors:errorList
        });
    }
    }

  render() {
    return (
      <div>
        <div className="card-header"> Profile Settings</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.changePassword} noValidate autoComplete="off">
          <br/>
          <div className="card-header">CHANGE PASSWORD</div>
          <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Password<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Password" name="password" id="in" value={this.state.fields.password} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["password"]}</span>
              </div>

              <div className="p-col-4">
                <div>Re-Enter Password<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Re-Enter Password" name="repassword" id="in" value={this.state.repassword} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["repassword"]}</span>
              </div>

            </div>
            <br />
            <span style={{ color: "red" }}>{this.state.errors["noMatch"]}</span>
            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Change Password</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default Profile;