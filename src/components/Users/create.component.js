import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {Dropdown} from 'primereact/dropdown';
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';
import {InputTextarea} from 'primereact/inputtextarea';
import {MultiSelect} from 'primereact/multiselect';
import {Calendar} from 'primereact/calendar';
import {Growl} from 'primereact/growl';

class create extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.addUser = this.addUser.bind(this);
    this.password = auth.generatePassword();
    var self = this;
    this.hubs = [];
    this.profession = [];
    this.emailSettings = [];

    this.state = {
      fields: {
        email: "",
        password: "",
        userRole: "",
        hubName: "",
        identityProof: "",
        personalDetails: {
          name: "",
          membershipType: "",
          phoneNumber: "",
          correspondingAddress: "",
          permanentAddress: "",
          profession: "",
          interests: "",
          maritalStatus: "",
          birthday: "",
          anniversary: "",
          bloodGroup: "",
          facebookId: "",
          instagramId: "",
          aboutYou: ""
        },
        paymentDetails: {
          paymentDate: "",
          paymentAmount: "",
          paymentType: ""
        }
      },
      errors: {},
      response:{}
    }

    this.hubs = auth.getPicklistDataList('hub');
    if(this.hubs.length===0) {
      auth.getData('hub').then(res => {
        let responseCopy = Object.assign({}, this.state.response);
        for(var i = 0; i<res.length;i++) {
          self.hubs.push({label: res[i].hubName, value: res[i].hubName});
        }
        responseCopy['hubs'] = self.hubs;
        this.setState({response:responseCopy});
      })
    } else {
      this.state.response['hubs'] = this.hubs;
    }

    this.membershipTypes = [
      {label: 'Honorary', value: 'Honorary'},
      {label: 'Annual', value: 'Annual'},
      {label: 'Half Yearly', value: 'Half Yearly'},
    ]

    this.profession = auth.getPicklistDataList('occupation');
    if(this.profession.length===0) {
      auth.getData('occupation').then(res => {
        let responseCopy = Object.assign({}, this.state.response);
        for(var i = 0; i<res.length;i++) {
          self.profession.push({label: res[i].name, value: res[i].name});
        }
        responseCopy['profession'] = self.profession;
        this.setState({response:responseCopy});
      })
    } else {
      this.state.response['profession'] = this.profession;
    }


    this.interests = [
      {label: 'Music', value: 'Music'},
      {label: 'Theater', value: 'Theater'},
      {label: 'Painting', value: 'Painting'},
      {label: 'Social Work', value: 'Social Work'},
      {label: 'Languages', value: 'Languages'},
      {label: 'Dance', value: 'Dance'},
      {label: 'Writing', value: 'Writing'},
      {label: 'DIY\'s', value: 'DIY\'s'},
      {label: 'Others', value: 'Others'}

    ]

    this.maritalStatus = [
      {label: 'Married', value: 'Married'},
      {label: 'Single', value: 'Single'},
      {label: 'Rather Not Say', value: 'Rather Not Say'}
    ]

    this.personalDetailsRequiredFields = [
      "membershipType","name","phoneNumber","correspondingAddress","permanentAddress","maritalStatus","birthday"
    ]

    this.otherRequiredFields = [
      "email","hubName","userRole"
    ]

    this.identityProof = [
      {label: 'Aadhar Card', value: 'Aadhar Card'},
      {label: 'Passport', value: 'Passport'},
      {label: 'Voter ID', value: 'Voter ID'}
    ]

    this.paymentTypes = [
      {label: 'Cheque', value: 'Cheque'},
      {label: 'Cash', value: 'Cash'},
      {label: 'Credit / Debit Card', value: 'Credit / Debit Card'},
      {label: 'Bank Transfer', value: 'Bank Transfer'},
      {label: 'Digital Wallet', value: 'Digital Wallet'}
    ]

    this.userRoles = [
      {label: 'User', value: 'User'},
      {label: 'Admin', value: 'Admin'},
      {label: 'SuperAdmin', value: 'SuperAdmin'}
    ]

    this.emailSettings = auth.getPicklistDataList('email');
    if(this.emailSettings.length===0) {
      auth.getPicklistData('email');
    }
  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    for(var i=0;i<this.personalDetailsRequiredFields.length;i++) {
      if(!fields["personalDetails"][this.personalDetailsRequiredFields[i]]){
        formIsValid = false;
        let combine = "personalDetails."+this.personalDetailsRequiredFields[i];
        errors[combine] = "Required Field";
     }
    }

    for(i=0;i<this.otherRequiredFields.length;i++) {
      if(!fields[this.otherRequiredFields[i]]){
        formIsValid = false;
        errors[this.otherRequiredFields[i]] = "Required Field";
     }
    }

    //Email
    if(fields["email"] !== ""){
       let lastAtPos = fields["email"].lastIndexOf('@');
       let lastDotPos = fields["email"].lastIndexOf('.');

       if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
          formIsValid = false;
          errors["email"] = "Email is not valid";
        }
   }  
   let fieldCopy = Object.assign({}, this.state.fields);
   fieldCopy["password"] = this.password;
   //this.setState({fields:fieldCopy});

   this.setState({fields:fieldCopy,errors: errors});
   return formIsValid;
}

showSuccess() {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: 'User Created Successfully !!'});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError() {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'User Creation Failed !!'});
}

  addUser(e) {
    var self = this;
    e.preventDefault();
    if(this.handleValidation()){
      auth.postData('users',this.state.fields).then(res => {
        if(res!==undefined) {
          if(res._id) {
            auth.createEmailData(this.state.fields,'new');
            this.showSuccess();
            window.location = '/#/users';
          }
        } else {
          this.showError();
        }
    });
      // alert("Form submitted");
   }else{
      this.showWarn();
   }
  }

  cancel() {
    window.location = '/#/users';
  }

  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    if(e.target.name==='personalDetails.birthday' || e.target.name==='personalDetails.anniversary' || e.target.name==='paymentDetails.paymentDate') {
      var date = e.target.value;
      inputValue = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear()
    }
    let errorList = Object.assign({}, this.state.errors);
    if(inputName.indexOf('.')>-1) {
      var object = inputName.split('.')[0];
      var field = inputName.split('.')[1];
      if(object==='personalDetails') {
        let personalDetailsCopy = Object.assign({}, this.state.fields);
        personalDetailsCopy.personalDetails[field] = inputValue;
        let combine = "personalDetails."+field;
        errorList[combine] ="";
        this.setState({fields:personalDetailsCopy,errors:errorList});
       // this.setState({errors:errorList});
      } else if(object==='paymentDetails') {
        let paymentDetailsCopy = Object.assign({}, this.state.fields);
        paymentDetailsCopy.paymentDetails[field] = inputValue;
        let combine = "paymentDetails."+field;
        errorList[combine] ="";
        this.setState({fields:paymentDetailsCopy,errors:errorList});
      }
    } else {
      errorList[inputName] ="";
      let otherCopy = Object.assign({}, this.state.fields);
      otherCopy[inputName] = inputValue;
      this.setState({
        fields: otherCopy,errors:errorList
      });
    }
  }


  handleClose(choice) {
    var self = this;
    //var selectedData = this.gridApi.getSelectedRows();
    //var params = { force: true };
    if(choice) {
        auth.deleteData('users',this.state.userId).then(res => {
            self.state.rowData.splice(this.state.id,1);
            self.gridApi.setRowData(self.state.rowData);
            //self.gridApi.refreshCells(params)
            //self.gridApi.updateRowData({ remove: selectedData })
            //console.log('deleted successfully')
        })
    } else {
        this.setState({id : null});
    }
    this.setState({setOpen : false});
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> Add User</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.addUser} noValidate autoComplete="off">
          <br/>
          <div className="card-header">MEMBERSHIP DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-3">
                <div>Hub Name</div>
                <div className="p-inputgroup">
                  <Dropdown name="hubName" placeholder="Select Hub Name" id="select" value={this.state.fields.hubName} options={this.state.response.hubs} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["hubName"]}</span>
              </div>

              <div className="p-col-3">
                <div>User Role<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Dropdown name="userRole" placeholder="Select User Role" id="select" value={this.state.fields.userRole} options={this.userRoles} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["userRole"]}</span>
              </div>

              <div className="p-col-3">
                <div>Membership Type<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Dropdown name="personalDetails.membershipType" placeholder="Select Membership Type" id="select" value={this.state.fields.personalDetails.membershipType} options={this.membershipTypes} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.membershipType"]}</span>
              </div>

            </div>
            <br />
            <div className="card-header">MEMBER DETAILS</div>
            <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Full Name<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Full Name" name="personalDetails.name" id="in" value={this.state.fields.personalDetails.name} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.name"]}</span>
              </div>

              <div className="p-col-4">
                <div>Phone Number<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputMask mask="9999999999" name="personalDetails.phoneNumber" value={this.state.fields.personalDetails.phoneNumber} placeholder="999-99-9999" onChange={this.updateState}></InputMask>
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.phoneNumber"]}</span>
              </div>

            </div>
            <br />

            <div className="p-grid p-fluid">
              <div className="p-col-12 ">
                <div>Corresponding Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="Corresponding Address" name="personalDetails.correspondingAddress" rows={2} cols={30} value={this.state.fields.personalDetails.correspondingAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.correspondingAddress"]}</span>
              </div>
            </div>
            <br />

            <div className="p-grid p-fluid">
              <div className="p-col-12">
                <div>Permanent Address<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="Permanent Address" name="personalDetails.permanentAddress" rows={2} cols={30} value={this.state.fields.personalDetails.permanentAddress} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.permanentAddress"]}</span>
              </div>
            </div>
            <br />

            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Email ID<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText keyfilter="email" name="email" value={this.state.fields.email} placeholder="abc@xyz.com" onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["email"]}</span>
              </div>

              <div className="p-col-3">
                <div>Profession</div>
                <div className="p-inputgroup">
                  <Dropdown placeholder="Select Profession" name="personalDetails.profession" id="select" value={this.state.fields.personalDetails.profession} options={this.state.response.profession} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.profession"]}</span>
              </div>

              <div className="p-col-4">
                <div>Interests</div>
                <div className="p-inputgroup">
                  <MultiSelect name="personalDetails.interests" value={this.state.fields.personalDetails.interests} options={this.interests} onChange={this.updateState} style={{ minWidth: '10em' }} filter={true} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.interests"]}</span>
              </div>

            </div>
            <br />

            <div className="p-grid p-fluid">

              <div className="p-col-3">
                <div>Marital Status<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Dropdown placeholder="Select Marital Status" name="personalDetails.maritalStatus" id="select" value={this.state.fields.personalDetails.maritalStatus} options={this.maritalStatus} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.maritalStatus"]}</span>
              </div>

              <div className="p-col-4">
                <div>Birthday<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Calendar dateFormat="dd/mm/yy" placeholder="Birthday" name="personalDetails.birthday" value={this.state.fields.personalDetails.birthday} onChange={this.updateState} monthNavigator={true} yearNavigator={true} yearRange="1900:2100" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.birthday"]}</span>
              </div>

              <div className="p-col-4">
                <div>Anniversary</div>
                <div className="p-inputgroup">
                  <Calendar placeholder="Anniversary" name="personalDetails.anniversary" value={this.state.fields.personalDetails.anniversary} onChange={this.updateState} monthNavigator={true} yearNavigator={true} yearRange="1900:2100" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.anniversary"]}</span>
              </div>

            </div>
            <br />

            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Blood Group</div>
                <div className="p-inputgroup">
                  <InputText placeholder="Blood Group" name="personalDetails.bloodGroup" id="in" value={this.state.fields.personalDetails.bloodGroup} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.bloodGroup"]}</span>
              </div>

              <div className="p-col-4">
                <div><i className="fa fa-facebook-square fa-lg"></i> ID</div>
                <div className="p-inputgroup">
                  <InputText placeholder="Facebook Id" name="personalDetails.facebookId" id="in" value={this.state.fields.personalDetails.facebookId} onChange={this.updateState} />

                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.facebookId"]}</span>
              </div>

              <div className="p-col-4">
                <div><i className="fa fa-instagram fa-lg"></i> ID</div>
                <div className="p-inputgroup">
                  <InputText placeholder="Intagram Id" name="personalDetails.instagramId" id="in" value={this.state.fields.personalDetails.instagramId} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.instagramId"]}</span>
              </div>


            </div>
            <br />

            <div className="p-grid p-fluid">

              <div className="p-col-12">
                <div>About the Member</div>
                <div className="p-inputgroup">
                  <InputTextarea placeholder="About the Member" name="personalDetails.aboutYou" rows={2} cols={30} value={this.state.fields.personalDetails.aboutYou} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["personalDetails.aboutYou"]}</span>
              </div>

            </div>
            <br />
            <div className="card-header">IDENTITY PROOF</div>
            <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-3">
                <div>Identity Proof<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <Dropdown placeholder="Select Identity Proof" name="identityProof" id="select" value={this.state.fields.identityProof} options={this.identityProof} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["identityProof"]}</span>
              </div>

            </div>
            <br />

            <div className="card-header">PAYMENT DETAILS</div>
            <br/>
            <div className="p-grid p-fluid">

              <div className="p-col-4">
                <div>Payment Date</div>
                <div className="p-inputgroup">
                  <Calendar placeholder="Payment Date" name="paymentDetails.paymentDate" value={this.state.fields.paymentDetails.paymentDate} onChange={this.updateState} monthNavigator={true} yearNavigator={true} yearRange="1900:2100" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["paymentDetails.paymentDate"]}</span>
              </div>

              <div className="p-col-4">
                <div>Payment Amount</div>
                <div className="p-inputgroup">
                  <InputText placeholder="Payment Amount" keyfilter="money" name="paymentDetails.paymentAmount" value={this.state.fields.paymentDetails.paymentAmount} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["paymentDetails.paymentAmount"]}</span>
              </div>

              <div className="p-col-3">
                <div>Payment Type</div>
                <div className="p-inputgroup">
                  <Dropdown placeholder="Select Payment Type" name="paymentDetails.paymentType" id="select" value={this.state.fields.paymentDetails.paymentType} options={this.paymentTypes} onChange={this.updateState}
                    filter={true} filterPlaceholder="Search" filterBy="label,value" />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["paymentDetails.paymentType"]}</span>
              </div>



            </div>




            <br />

            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Add User</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default create;