import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputText} from 'primereact/inputtext';
import {Growl} from 'primereact/growl';

class create extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.addOccupation = this.addOccupation.bind(this);
    
    this.state = {
      fields: {
        name:""
      },
      errors: {}
    }

    this.otherRequiredFields = [
      "name"
    ]

  }
  
  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    
      if(!fields["name"]){
        formIsValid = false;
        errors["name"] = "Required Field";
      }


   this.setState({errors: errors});
   return formIsValid;
}

showSuccess() {
  this.growl.show({severity: 'success', summary: 'Success Message', detail: 'Occupation Created Successfully !!'});
}

showInfo() {
  this.growl.show({severity: 'info', summary: 'Info Message', detail: 'Validation Failed'});
}

showWarn() {
  this.growl.show({severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!'});
}

showError() {
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Occupation Creation Failed !!'});
}

addOccupation(e) {
    e.preventDefault();
    if(this.handleValidation()){
      auth.postData('occupation',this.state.fields).then(res => {
        if(res!==undefined) {
          if(res._id) {
            this.showSuccess();
            auth.getPicklistData('occupation');
            window.location = '/#/occupations';
          }
        } else {
          this.showError();
        }
    });
   } else {
      this.showWarn();
   }
  }

  cancel() {
    window.location = '/#/occupations';
  }

  updateState(e) {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    let errorList = Object.assign({}, this.state.errors);
    errorList[inputName] ="";
    let otherCopy = Object.assign({}, this.state.fields);
    otherCopy[inputName] = inputValue;
    this.setState({
    fields: otherCopy,errors:errorList
    });
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> Add Occupation</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" onSubmit={this.addOccupation} noValidate autoComplete="off">
          <br/>
          <div className="card-header">OCCUPATION DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-4">
                <div>Occupation<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText placeholder="Occupation" name="name" id="in" value={this.state.fields.name} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["name"]}</span>
              </div>
             
            </div>
            <br />

            <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Add Occupation</Button>
          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default create;