import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import {InputText} from 'primereact/inputtext';
import {Growl} from 'primereact/growl';

class view extends Component {

  constructor(props) {
    super(props);
    const occupationId = this.props.match.params.id;
    var self =this;

    this.state = {
      fields: {
        name:""
      },
      errors: {}
    }

    auth.getDataById('occupation',occupationId).then( res => {
        if(res!==undefined) {
            self.setState({
                fields: {
                    occupationId: occupationId,
                    name: res.name
                },
                errors: {}
              })
        }
    })

  }
  
  cancel() {
    window.location = '/#/occupations';
  }

  render() {
    return (
      <div>
        <div >
          <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
        </div>
        <br />
        <div className="card-header"> View Occupation</div>
        <Growl  ref={(el) => this.growl = el}  position="topright" style={{top : '75px',right: '5px'}}/>
        <Container maxWidth="lg" className="container">
          <form name="formEl" noValidate autoComplete="off">
          <br/>
          <div className="card-header">OCCUPATION DETAILS</div>
          <br/>
            <div className="p-grid p-fluid">

            <div className="p-col-4">
                <div>Occupation<span style={{ color: "red" }}>*</span></div>
                <div className="p-inputgroup">
                  <InputText disabled placeholder="Occupation" name="name" id="in" value={this.state.fields.name} onChange={this.updateState} />
                </div>
                <span style={{ color: "red" }}>{this.state.errors["name"]}</span>
              </div>
             
            </div>
            <br />

          </form>
          <br />
          <br />

        </Container>
      </div>
     
    );
  }
}

export default view;