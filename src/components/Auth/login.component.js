import React, { Component } from 'react';
import { Card, CardBody, CardGroup, Col, Container, Form, InputGroup, Row } from 'reactstrap';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import config from '../../config';
import logo from '../../assets/img/brand/icon.webp';

import TextField from '@material-ui/core/TextField';
class Login extends Component {

  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
        fields: {
          username: '',
          password: ''
        },
        errors: {}
    }
}
updateState(e) {
  let inputName = e.target.name;
  let inputValue = e.target.value;
  let fieldCopy = Object.assign({}, this.state.fields);
  let errorList = Object.assign({}, this.state.errors);
  errorList[inputName] = "";
  fieldCopy[inputName] = inputValue;
  this.setState({
    fields: fieldCopy,errors:errorList
  });
}

handleValidation(){
  let fields = this.state.fields;
  let errors = {};
  let formIsValid = true;

  if(!fields["username"]) {
    formIsValid = false;
    errors["username"] = "Required Field";
  }

  if(!fields["password"]) {
    formIsValid = false;
    errors["password"] = "Required Field";
  }

  //Email
  if(fields["username"] !== ""){
     let lastAtPos = fields["username"].lastIndexOf('@');
     let lastDotPos = fields["username"].lastIndexOf('.');

     if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["username"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["username"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["username"] = "Email is not valid";
      }
 }  

 this.setState({errors: errors});
 return formIsValid;
}

onSubmit(e) {
  e.preventDefault();
  if(this.handleValidation()) {
    console.log(`The values are ${this.state.username}, ${this.state.password}`)
    const obj = {
        email: this.state.fields.username,
        password: this.state.fields.password
      };

    var headers = {
        'Content-Type': 'application/json'
    }
    axios.post(config.ApiUrl+'auth', obj,{ headers : headers }).then(res => {
        console.log(res);
        if(res.status===200 && res.data.role!=='User') {
          localStorage.setItem('x-access-token',JSON.stringify({token:res.data.access_token,name:res.data.name,email:res.data.email,role:res.data.role,id:res.data.id}));
          window.location = '/';
        } else {
          let errors = {};
          errors["invalid"] = "SuperAdmin/Admins Only";
          this.setState({errors: errors});
        }
    }).catch(error => {
      if(error.response.status===400) {
        let errors = {};
        errors["invalid"] = "Invalid Email / Password";
        this.setState({errors: errors});
      }
    });
  }
}
  render() {
    return (
      <div className="app flex-row align-items-center backgroundImage">
        <Container className="noContainer">
          <Row className="justify-content-center">
            <Col md="4">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.onSubmit}>
                      {/* <h1 className="center">Login</h1> */}
                      <img src={logo} alt="logo" className="login-Logo"/>
                      <p className="text-muted center">Sign In to your account</p>
                      <InputGroup className="mb-3 center">
                       
                        <TextField
        id="outlined-name"
        label="Email"
        name="username"
        value={this.state.fields.username}
        onChange={this.updateState}
        margin="normal"
        variant="outlined"
        autoFocus
      />
      <span style={{ color: "red" }}>{this.state.errors["username"]}</span>
                      </InputGroup>
                      <InputGroup className="mb-6">
                        <TextField
        id="outlined-name"
        label="Password"
        type="password"
        name="password"
        value={this.state.fields.password}
        onChange={this.updateState}
        margin="normal"
        variant="outlined"
      />
      <span style={{ color: "red" }}>{this.state.errors["password"]}</span>

                      </InputGroup>
                      <br/>
                      <Row>
                      <span style={{ color: "red" }}>{this.state.errors["invalid"]}</span>
                      </Row>
                      <br/>
                      <Row>
                      <InputGroup className="mb-6 button-center">
                        <Button type="submit" style={{width:'50px'}} variant="outlined" color="primary" >
        Login
      </Button>
      </InputGroup>
                        {/* <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col> */}
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                {/* <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card> */}
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
      
    );
  }
}

export default Login;
