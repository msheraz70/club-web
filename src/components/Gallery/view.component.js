import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { InputTextarea } from 'primereact/inputtextarea';
import config from '../../config';

class view extends Component {

    constructor(props) {
        super(props);
        const galleryId = this.props.match.params.id;
        var self = this;

        this.state = {
            fields: {
                galleryId: galleryId,
                galleryName: "",
                galleryDescription: "",
                galleryImages: []
            },
            errors: {}
        }

        auth.getDataById('gallery',galleryId).then( res => {
            if(res!==undefined) {
                self.setState({
                    fields: {
                        galleryId: galleryId,
                        galleryName: res.galleryName,
                        galleryDescription: res.galleryDescription,
                        galleryImages: res.galleryImages
                    },
                    errors: {}
                  })
            }
        })
    
      }

    cancel() {
        window.location = '/#/galleries';
    }

    render() {
        return (
            <div>
                <div >
                    <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
                </div>
                <br />
                <div className="card-header"> View Gallery</div>
                <Growl ref={(el) => this.growl = el} position="topright" style={{ top: '75px', right: '5px' }} />
                <Container maxWidth="lg" className="container">
                    <form name="formEl" noValidate autoComplete="off">
                        <br />
                        <div className="card-header">GALLERY DETAILS</div>
                        <br />
                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Gallery Name<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputText disabled placeholder="Gallery Name" name="galleryName" id="galleryName" value={this.state.fields.galleryName} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryName"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Gallery Description<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputTextarea disabled placeholder="Gallery Description" name="galleryDescription" rows={2} cols={30} value={this.state.fields.galleryDescription} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryDescription"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="card-header">GALLERY IMAGES</div>
                        <br />

                        <div className={this.state.fields.galleryImages.length===0 ? 'hidden' : ''}>
              Uploaded Images
              <br/>
            {this.state.fields.galleryImages.map((i, index) =>
               <div key={index}>
               <div className={i===null ? 'hidden' : ''}><img src={config.Url+i} alt={i} style={{width:'100px',height:'100px'}}/>
       </div>
                <br/>
              </div>)}
            </div>
                    </form>
                    <br />
                    <br />

                </Container>
            </div>

        );
    }
}

export default view;