import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { InputTextarea } from 'primereact/inputtextarea';
import { FileUpload } from 'primereact/fileupload';

const Initialfiles = [];

class create extends Component {

    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.addGallery = this.addGallery.bind(this);
        this.files = this.files.bind(this);
        this.clearFiles = this.clearFiles.bind(this);

        this.state = {
            fields: {
                galleryName: "",
                galleryDescription: "",
                galleryImages: []
            },
            errors: {},
            files: Initialfiles
        }

        this.otherRequiredFields = [
            "galleryName", "galleryDescription"
        ]

    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        for (var i = 0; i < this.otherRequiredFields.length; i++) {
            if (!fields[this.otherRequiredFields[i]]) {
                formIsValid = false;
                errors[this.otherRequiredFields[i]] = "Required Field";
            }
        }

        if (this.state.files.length === 0) {
            formIsValid = false;
            errors["fileError"] = "Please Upload Images";
        }


        this.setState({ errors: errors });
        return formIsValid;
    }

    showSuccess() {
        this.growl.show({ severity: 'success', summary: 'Success Message', detail: 'Gallery Created Successfully !!' });
    }

    showInfo() {
        this.growl.show({ severity: 'info', summary: 'Info Message', detail: 'Validation Failed' });
    }

    showWarn() {
        this.growl.show({ severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!' });
    }

    showError() {
        this.growl.show({ severity: 'error', summary: 'Error Message', detail: 'Gallery Creation Failed !!' });
    }

    uploadImages() {
        var self = this;
        var data = new FormData();
        data.append('folderName', 'Gallery');
        data.append('name', this.state.fields.galleryName);
        for (const file of this.state.files) {
            data.append('files[]', file, file.name);
        }
        auth.postImage('uploadFiles', data).then(res => {
            let fieldCopy = Object.assign({}, this.state.fields);
            if (res.path !== []) {
                fieldCopy['galleryImages'] = res.path;
                self.setState({ fields: fieldCopy })
                auth.postData('gallery', this.state.fields).then(res => {
                    if (res !== undefined) {
                        if (res._id) {
                            this.showSuccess();
                            setTimeout(
                                function () {
                                    window.location = '/#/galleries';
                                },
                                1000
                            );
                        }
                    } else {
                        this.showError();
                    }
                });
            } else {
                this.showError();
            }
        });
    }

    addGallery(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            this.uploadImages();
        } else {
            this.showWarn();
        }
    }

    cancel() {
        window.location = '/#/galleries';
    }

    updateState(e) {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        let errorList = Object.assign({}, this.state.errors);
        errorList[inputName] = "";
        let otherCopy = Object.assign({}, this.state.fields);
        otherCopy[inputName] = inputValue;
        this.setState({
            fields: otherCopy, errors: errorList
        });
    }

    files(e) {
        let errorList = Object.assign({}, this.state.errors);
        errorList['fileError'] = "";
        this.setState({ files: [...Initialfiles, ...e.originalEvent.target.files], errors: errorList });
    }

    clearFiles() {
        this.setState({ files: [] });
    }

    render() {
        return (
            <div>
                <div >
                    <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
                </div>
                <br />
                <div className="card-header"> Add Gallery</div>
                <Growl ref={(el) => this.growl = el} position="topright" style={{ top: '75px', right: '5px' }} />
                <Container maxWidth="lg" className="container">
                    <form name="formEl" onSubmit={this.addGallery} noValidate autoComplete="off">
                        <br />
                        <div className="card-header">GALLERY DETAILS</div>
                        <br />
                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Gallery Name<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputText placeholder="Gallery Name" name="galleryName" id="galleryName" value={this.state.fields.galleryName} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryName"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Gallery Description<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputTextarea placeholder="Gallery Description" name="galleryDescription" rows={2} cols={30} value={this.state.fields.galleryDescription} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryDescription"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="card-header">GALLERY IMAGES</div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Upload Images<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <FileUpload name="files[]" url="https://www.primefaces.org/primereact/upload.php" onSelect={this.files} onClear={this.clearFiles}
                                        multiple={true} accept="image/*" maxFileSize={1000000} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["fileError"]}</span>
                            </div>

                        </div>
                        <br />

                        <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Add Gallery</Button>
                    </form>
                    <br />
                    <br />

                </Container>
            </div>

        );
    }
}

export default create;