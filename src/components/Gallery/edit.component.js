import React, { Component } from 'react';
import auth from '../../Services/AuthService';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { InputTextarea } from 'primereact/inputtextarea';
import { FileUpload } from 'primereact/fileupload';
import config from '../../config';

const Initialfiles = [];

class edit extends Component {

    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
        this.editGallery = this.editGallery.bind(this);
        this.files = this.files.bind(this);
        const galleryId = this.props.match.params.id;
        var self = this;

        this.state = {
            fields: {
                galleryId: galleryId,
                galleryName: "",
                galleryDescription: "",
                galleryImages: []
            },
            errors: {},
            files: Initialfiles
        }

        this.otherRequiredFields = [
            "galleryName", "galleryDescription"
        ]

        auth.getDataById('gallery',galleryId).then( res => {
            if(res!==undefined) {
                self.setState({
                    fields: {
                        galleryId: galleryId,
                        galleryName: res.galleryName,
                        galleryDescription: res.galleryDescription,
                        galleryImages: res.galleryImages
                    },
                    errors: {},
                    files: Initialfiles
                  })
            }
        })
    
      }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        for (var i = 0; i < this.otherRequiredFields.length; i++) {
            if (!fields[this.otherRequiredFields[i]]) {
                formIsValid = false;
                errors[this.otherRequiredFields[i]] = "Required Field";
            }
        }

        if (this.state.files.length === 0 && this.state.fields.galleryImages.length === 0) {
            formIsValid = false;
            errors["fileError"] = "Please Upload Images";
        }


        this.setState({ errors: errors });
        return formIsValid;
    }

    showSuccess(msg) {
        this.growl.show({ severity: 'success', summary: 'Success Message', detail: msg });
    }

    showInfo() {
        this.growl.show({ severity: 'info', summary: 'Info Message', detail: 'Validation Failed' });
    }

    showWarn() {
        this.growl.show({ severity: 'warn', summary: 'Warn Message', detail: 'Field Validation Failed !!' });
    }

    showError() {
        this.growl.show({ severity: 'error', summary: 'Error Message', detail: 'Gallery Updation Failed !!' });
    }

    uploadImages() {
        if(this.state.files.length!==0) {
            var self = this;
            var data = new FormData();
            data.append('folderName', 'Gallery');
            data.append('name', this.state.fields.galleryName);
            for (const file of this.state.files) {
                data.append('files[]', file, file.name);
            }
            auth.postImage('uploadFiles', data).then(res => {
                let fieldCopy = Object.assign({}, this.state.fields);
                if (res.path !== []) {
                    for(var i=0;i<res.path.length;i++) {
                        fieldCopy['galleryImages'].push(res.path[i]);
                    }
                    self.setState({ fields: fieldCopy })
                    auth.putData('gallery', this.state.fields).then(res => {
                        if (res !== undefined) {
                            if (res._id) {
                                this.showSuccess('Gallery Edited Successfully !!');
                                setTimeout(
                                    function () {
                                        window.location = '/#/galleries';
                                    },
                                    1000
                                );
                            }
                        } else {
                            this.showError();
                        }
                    });
                } else {
                    this.showError();
                }
            });
        } else {
            auth.putData('gallery', this.state.fields).then(res => {
                if (res !== undefined) {
                    if (res._id) {
                        this.showSuccess('Gallery Edited Successfully !!');
                        setTimeout(
                            function () {
                                window.location = '/#/galleries';
                            },
                            1000
                        );
                    }
                } else {
                    this.showError();
                }
            });
        }
    }

    editGallery(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            this.uploadImages();
        } else {
            this.showWarn();
        }
    }

    cancel() {
        window.location = '/#/galleries';
    }

    updateState(e) {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        let errorList = Object.assign({}, this.state.errors);
        errorList[inputName] = "";
        let otherCopy = Object.assign({}, this.state.fields);
        otherCopy[inputName] = inputValue;
        this.setState({
            fields: otherCopy, errors: errorList
        });
    }

    files(e) {
        let errorList = Object.assign({}, this.state.errors);
        errorList['fileError'] = "";
        this.setState({ files: [...Initialfiles, ...e.originalEvent.target.files], errors: errorList });
    }

    clearFiles() {
        this.setState({ files: [] });
    }

    deleteImage(imgPath) {
        var trueCopy = this.state.fields;
        var data = {
          "imagePath": imgPath
        };
        auth.deleteImage('uploadFiles',data).then(res => {
          if(res!==undefined) {
            var index = trueCopy.galleryImages.indexOf(imgPath);
            trueCopy.galleryImages.splice(index, 1);
            this.setState({
              fields: trueCopy
            })
            this.showSuccess('Image Deleted Successfully !');
          }
        });
      }

    render() {
        return (
            <div>
                <div >
                    <Button variant="contained" onClick={this.cancel} color="secondary" >Cancel</Button>
                </div>
                <br />
                <div className="card-header"> Edit Gallery</div>
                <Growl ref={(el) => this.growl = el} position="topright" style={{ top: '75px', right: '5px' }} />
                <Container maxWidth="lg" className="container">
                    <form name="formEl" onSubmit={this.editGallery} noValidate autoComplete="off">
                        <br />
                        <div className="card-header">GALLERY DETAILS</div>
                        <br />
                        <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Gallery Name<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputText disabled placeholder="Gallery Name" name="galleryName" id="galleryName" value={this.state.fields.galleryName} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryName"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Gallery Description<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <InputTextarea placeholder="Gallery Description" name="galleryDescription" rows={2} cols={30} value={this.state.fields.galleryDescription} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryDescription"]}</span>
                            </div>

                        </div>
                        <br />

                        {/* <div className="p-grid p-fluid">

                            <div className="p-col-4">
                                <div>Gallery Location</div>
                                <div className="p-inputgroup">
                                    <InputText placeholder="Gallery Location" name="galleryLocation" id="galleryLocation" value={this.state.fields.galleryLocation} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryLocation"]}</span>
                            </div>

                            <div className="p-col-4">
                                <div>Gallery Timings</div>
                                <div className="p-inputgroup">
                                    <InputText placeholder="Gallery Timings" name="galleryTimings" id="galleryTimings" value={this.state.fields.galleryTimings} onChange={this.updateState} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["galleryTimings"]}</span>
                            </div>

                        </div>
                        <br /> */}

                        <div className="card-header">GALLERY IMAGES</div>
                        <br />

                        <div className="p-grid p-fluid">

                            <div className="p-col-12">
                                <div>Upload Images<span style={{ color: "red" }}>*</span></div>
                                <div className="p-inputgroup">
                                    <FileUpload name="files[]" url="https://www.primefaces.org/primereact/upload.php" onSelect={this.files} onClear={this.clearFiles}
                                        multiple={true} accept="image/*" maxFileSize={1000000} />
                                </div>
                                <span style={{ color: "red" }}>{this.state.errors["fileError"]}</span>
                            </div>

                        </div>
                        <br />

                        <div className={this.state.fields.galleryImages.length===0 ? 'hidden' : ''}>
              Uploaded Images
              <br/>
            {this.state.fields.galleryImages.map((i, index) =>
               <div key={index}>
               <div className={i===null ? 'hidden' : ''}><img src={config.Url+i} alt={i} style={{width:'100px',height:'100px'}}/><Button type="button" variant="contained" style={{float:'right'}} onClick={() => this.deleteImage(i)} >
        Delete
       
      </Button></div>
                <br/>
              </div>)}
            </div>

                        <Button type="submit" className="submitbtn" variant="contained" color="secondary" >Edit Gallery</Button>
                    </form>
                    <br />
                    <br />

                </Container>
            </div>

        );
    }
}

export default edit;