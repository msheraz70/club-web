import  { Component } from 'react';
import axios from 'axios';
import config from '../config';
class AuthService extends Component  {
    constructor(props) {
        super(props);
        this.isLoggedIn = this.isLoggedIn.bind(this);
        this.getToken = this.getToken.bind(this);
        this.getUserRole = this.getUserRole.bind(this);
        this.getEmail = this.getEmail.bind(this);
        this.getId = this.getId.bind(this);
        this.logout = this.logout.bind(this);
        this.generatePassword = this.generatePassword.bind(this);
        this.getHeaders = this.getHeaders.bind(this);
        this.getData = this.getData.bind(this);
        this.postData = this.postData.bind(this);
        this.deleteData = this.deleteData.bind(this);

        this.hubs = [];
        this.occupations = [];
        this.emailSettings = [];
    }

    isLoggedIn() {
        var loggedInDetails = JSON.parse(localStorage.getItem('x-access-token'));
        if(loggedInDetails) {
            this.accessToken = loggedInDetails.token;
            this.userRole = loggedInDetails.userRole;
            this.email = loggedInDetails.email;
            this.id = loggedInDetails.id;
            return true;
        } else {
            return false;
        }
    }

    getToken() {
        return this.accessToken;
    }

    getUserRole() {
        return this.userRole;
    }

    getEmail() {
        return this.email;
    }

    getId() {
        return this.id;
    }

    logout() {
        if(localStorage.getItem('x-access-token')) {
            localStorage.removeItem('x-access-token');
            this.accessToken = null;
            this.userRole = null;
            this.email = null;
            this.id = null;
            window.location = "/#/login/";
            window.location.reload();
        }
    }

    generatePassword() {
        var length = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    getHeaders() {
        var headers = {
            'Content-Type': 'application/json',
            'x-access-token': this.accessToken
        }
        return headers;
    }

    //Http Get
    getData(apiName) {
        return axios.get(config.ApiUrl+apiName, { headers: this.getHeaders() }).then(res => {
            console.log(res);
            if (res.status === 200) {
              return res.data.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.status === 400) {
                console.log('Could Not Get Data !!');
            }
        });
    }

    //Http Get By Id
    getDataById(apiName,id) {
        return axios.get(config.ApiUrl+apiName+'/'+ id, { headers: this.getHeaders() }).then(res => {
            console.log(res);
            if (res.status === 200) {
              return res.data.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.status === 400) {
                console.log('Could Not Get Data !!');
            }
        });
    }

    //Http Put
    putData(apiName,data) {
        return axios.put(config.ApiUrl+apiName,data, { headers: this.getHeaders() }).then(res => {
            console.log(res);
            if (res.status === 200) {
              return res.data.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.status === 400) {
                console.log('Could Not Put Data !!');
            }
        });
    }

    //Http Post
    postData(apiName,data) {
        return axios.post(config.ApiUrl+apiName,data, { headers: this.getHeaders() }).then(res => {
            console.log(res);
            if (res.status === 200) {
              return res.data.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.data.status === 400) {
                return error.response.data;
            } else if(error.response.data.status === 404) {
                return error.response.data;
            }
        });
    }

    //Http Delete
    deleteData(apiName,id) {
        return axios.delete(config.ApiUrl+apiName+'/'+id, { headers: this.getHeaders() }).then(res => {
            console.log(res);
            if (res.status === 200) {
              return res.data.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.status === 400) {
                console.log('Could Not Delete Data !!');
            }
        });
    }

    //Upload Image
    postImage(apiName,data) {
        return axios.post(config.ApiUrl+apiName,data, { headers: {'Content-Type': undefined }}).then(res => {
            console.log(res); 
            if (res.status === 200) {
              return res.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.data.status === 400) {
                return error.response.data;
            } else if(error.response.data.status === 404) {
                return error.response.data;
            }
        });
    }

    //Delete Image
    deleteImage(apiName,data) {
        return axios.delete(config.ApiUrl+apiName, { data:data }).then(res => {
            console.log(res); 
            if (res.status === 200) {
              return res.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.data.status === 400) {
                return error.response.data;
            } else if(error.response.data.status === 404) {
                return error.response.data;
            }
        });
    }

    //Generate Mail Payload 
    createEmailData(payload,type) {
        var res;
        if(type==='new') {
            res = {
                to: payload.email,
                userData:{
                    name: payload.personalDetails.name,
                    password: payload.password
                },
                mailDetails: {
                    from: this.emailSettings[0].from,
                    userName: this.emailSettings[0].userName,
                    password: this.emailSettings[0].password,
                    subject: this.emailSettings[0].newMemberMailSubject,
                    message: this.emailSettings[0].newMemberMailBody,
                    htmlMessage: this.emailSettings[0].newMemberMailBody
                }
            }
        } else {
            res = {
                to: payload.email,
                userData:{
                    name: payload.personalDetails.name,
                    password: payload.password
                },
                mailDetails: {
                    from: this.emailSettings[0].from,
                    userName: this.emailSettings[0].userName,
                    password: this.emailSettings[0].password,
                    subject: this.emailSettings[0].forgotPasswordMailSubject,
                    message: this.emailSettings[0].forgotPasswordMailBody,
                    htmlMessage: this.emailSettings[0].forgotPasswordMailBody
                }
            }
        }
        this.sendMail(res);
        
      }

    //Send Mail
    sendMail(payLoad) {
        return axios.post(config.ApiUrl+'sendmail', payLoad, { headers: this.getHeaders() }).then(res => {
            console.log(res);
            if (res.status === 200) {
              return res.data.data;
            }
        }).catch(error => {
            if(error.response===undefined) {
                console.log('Error');
            } else if (error.response.status === 400) {
                console.log('Could Not Mail Data !!');
            }
        });
    }

    //Get Required Picklists
    getPicklistData(picklist) {
        var self = this;
        self.hubs = [];
        self.occupations = [];
        self.emailSettings = [];
        auth.getData(picklist).then( res => {
          if(res!==undefined) {
            for(var i = 0; i<res.length;i++) {
                if(picklist === 'hub') {
                    self.hubs.push({label: res[i].hubName, value: res[i].hubName});
                } else if(picklist === 'occupation') {
                    self.occupations.push({label: res[i].name, value: res[i].name});
                } else if(picklist === 'email') {
                    self.emailSettings.push(res[i]);
                }
            }
          }
      })
    }

    getPicklistDataList(picklist) {
        if(picklist === 'hub') {
            return this.hubs;
        } else if(picklist === 'occupation') {
            return this.occupations;
        } else if(picklist === 'email') {
            return this.emailSettings;
        }
    }
}

const auth = new AuthService();
export default auth;
