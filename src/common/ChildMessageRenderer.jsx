import React, { Component } from 'react';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
export default class ChildMessageRenderer extends Component {
    constructor(props) {
        super(props);

        this.invokeView = this.invokeView.bind(this);
        this.invokeEdit = this.invokeEdit.bind(this);
        this.invokeDelete = this.invokeDelete.bind(this);
    }

    invokeView() {
        this.props.context.componentParent.view(this.props.node)
    }

    invokeEdit() {
        this.props.context.componentParent.edit(this.props.node)
    }

    invokeDelete() {
        this.props.context.componentParent.delete(this.props.node)
    }

    render() {
        return (
            <span>
                <span>
                <Fab color="secondary" onClick={this.invokeView} aria-label="edit" style={{ height: '35px', width: '35px', margin: '5px'}}>
            <Icon style={{ fontSize: '17px' }}>visibility_icon</Icon>
          </Fab>
          </span>
            <span>
                <Fab color="primary" onClick={this.invokeEdit} aria-label="edit" style={{ height: '35px', width: '35px', margin: '5px'}}>
            <Icon style={{ fontSize: '17px' }}>edit_icon</Icon>
          </Fab>
          </span>
          <span>
                <Fab color="secondary" onClick={this.invokeDelete} aria-label="edit" style={{ height: '35px', width: '35px', margin: '5px'}}>
            <Icon style={{ fontSize: '17px' }}>delete_icon</Icon>
          </Fab>
          </span>
          {/* <span><Button  variant="contained" onClick={this.invokeEdit} color="primary" >Edit</Button> </span>
         <span><Button variant="contained" onClick={this.invokeDelete} color="secondary" >Delete</Button></span> */}
         </span>
        );
    }
};