import React from 'react';

const Breadcrumbs = React.lazy(() => import('./views/Base/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/Base/Cards'));
const Carousels = React.lazy(() => import('./views/Base/Carousels'));
const Collapses = React.lazy(() => import('./views/Base/Collapses'));
const Dropdowns = React.lazy(() => import('./views/Base/Dropdowns'));
const Forms = React.lazy(() => import('./views/Base/Forms'));
const Jumbotrons = React.lazy(() => import('./views/Base/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/Base/ListGroups'));
const Navbars = React.lazy(() => import('./views/Base/Navbars'));
const Navs = React.lazy(() => import('./views/Base/Navs'));
const Paginations = React.lazy(() => import('./views/Base/Paginations'));
const Popovers = React.lazy(() => import('./views/Base/Popovers'));
const ProgressBar = React.lazy(() => import('./views/Base/ProgressBar'));
const Switches = React.lazy(() => import('./views/Base/Switches'));
const Tables = React.lazy(() => import('./views/Base/Tables'));
const Tabs = React.lazy(() => import('./views/Base/Tabs'));
const Tooltips = React.lazy(() => import('./views/Base/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/Buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/Buttons/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/Buttons/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/Buttons/Buttons'));
const Charts = React.lazy(() => import('./views/Charts'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/Icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/Icons/Flags'));
const FontAwesome = React.lazy(() => import('./views/Icons/FontAwesome'));
const SimpleLineIcons = React.lazy(() => import('./views/Icons/SimpleLineIcons'));
const Alerts = React.lazy(() => import('./views/Notifications/Alerts'));
const Badges = React.lazy(() => import('./views/Notifications/Badges'));
const Modals = React.lazy(() => import('./views/Notifications/Modals'));
const Colors = React.lazy(() => import('./views/Theme/Colors'));
const Typography = React.lazy(() => import('./views/Theme/Typography'));
const Widgets = React.lazy(() => import('./views/Widgets/Widgets'));

//Home
const Home = React.lazy(() => import('./components/Home/Home.component'));

//User
const User = React.lazy(() => import('./components/Users/index.component'));
const createUsers = React.lazy(() => import('./components/Users/create.component'));
const editUser = React.lazy(() => import('./components/Users/edit.component'));
const viewUser = React.lazy(() => import('./components/Users/view.component'));

//Event
const Event = React.lazy(() => import('./components/Events/index.component'));
const createEvent = React.lazy(() => import('./components/Events/create.component'));
const editEvent = React.lazy(() => import('./components/Events/edit.component'));
const viewEvent = React.lazy(() => import('./components/Events/view.component'));

//Hub
const Hub = React.lazy(() => import('./components/Hubs/index.component'));
const createHub = React.lazy(() => import('./components/Hubs/create.component'));
const editHub = React.lazy(() => import('./components/Hubs/edit.component'));
const viewHub = React.lazy(() => import('./components/Hubs/view.component'));

//Occupation
const Occupation = React.lazy(() => import('./components/Occupation/index.component'));
const createOccupation = React.lazy(() => import('./components/Occupation/create.component'));
const editOccupation = React.lazy(() => import('./components/Occupation/edit.component'));
const viewOccupation = React.lazy(() => import('./components/Occupation/view.component'));

//Facility
const Facility = React.lazy(() => import('./components/Facilities/index.component'));
const createFacility = React.lazy(() => import('./components/Facilities/create.component'));
const editFacility = React.lazy(() => import('./components/Facilities/edit.component'));
const viewFacility = React.lazy(() => import('./components/Facilities/view.component'));

//Gallery
const Gallery = React.lazy(() => import('./components/Gallery/index.component'));
const createGallery = React.lazy(() => import('./components/Gallery/create.component'));
const editGallery = React.lazy(() => import('./components/Gallery/edit.component'));
const viewGallery = React.lazy(() => import('./components/Gallery/view.component'));

//Message
const Message = React.lazy(() => import('./components/Message/index.component'));
// const editMessage = React.lazy(() => import('./components/Message/edit.component'));
const viewMessage = React.lazy(() => import('./components/Message/view.component'));

//Email Settings
const Email = React.lazy(() => import('./components/Settings/mail.component'));

//Profile
const Profile = React.lazy(() => import('./components/Home/Profile.component'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', exact: true, name: 'Theme', component: Colors },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', exact: true, name: 'Base', component: Cards },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/forms', name: 'Forms', component: Forms },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/charts', name: 'Charts', component: Charts },

  { path: '/home', name: 'Home', component: Home },

  //Users
  { path: '/users', exact: true,  name: 'Users', component: User },
  { path: '/users/create',  name: 'Create User', component: createUsers },
  { path: '/users/edit/:id',  name: 'Edit User', component: editUser },
  { path: '/users/view/:id',  name: 'View User', component: viewUser },

  //Events
  { path: '/events', exact: true,  name: 'Events', component: Event },
  { path: '/events/create',  name: 'Create Event', component: createEvent },
  { path: '/events/edit/:id',  name: 'Edit Event', component: editEvent },
  { path: '/events/view/:id',  name: 'View Event', component: viewEvent },

  //Hubs
  { path: '/hubs', exact: true,  name: 'Hubs', component: Hub },
  { path: '/hubs/create',  name: 'Create Hub', component: createHub },
  { path: '/hubs/edit/:id',  name: 'Edit Hub', component: editHub },
  { path: '/hubs/view/:id',  name: 'View Hub', component: viewHub },
  
  //Occupations
  { path: '/occupations', exact: true,  name: 'Occupations', component: Occupation },
  { path: '/occupations/create',  name: 'Create Occupation', component: createOccupation },
  { path: '/occupations/edit/:id',  name: 'Edit Occupation', component: editOccupation },
  { path: '/occupations/view/:id',  name: 'View Occupation', component: viewOccupation },

  //Facilities
  { path: '/facilities', exact: true,  name: 'Facilities', component: Facility },
  { path: '/facilities/create',  name: 'Create Facility', component: createFacility },
  { path: '/facilities/edit/:id',  name: 'Edit Facility', component: editFacility },
  { path: '/facilities/view/:id',  name: 'View Facility', component: viewFacility },

  //Gallery
  { path: '/galleries', exact: true,  name: 'Galleries', component: Gallery },
  { path: '/galleries/create',  name: 'Create Gallery', component: createGallery },
  { path: '/galleries/edit/:id',  name: 'Edit Gallery', component: editGallery },
  { path: '/galleries/view/:id',  name: 'View Gallery', component: viewGallery },

  //Message
  { path: '/messages', exact: true,  name: 'Messages', component: Message },
  // { path: '/messages/edit/:id',  name: 'Edit Message', component: editMessage },
   { path: '/messages/view/:id',  name: 'View Message', component: viewMessage },

  //Mail Settings
  { path: '/emailSettings',  name: 'Email Settings', component: Email },

  //Profile 
  { path: '/profile',  name: 'Profile Settings', component: Profile },
];

export default routes;
