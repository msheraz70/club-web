import  { Component } from 'react';

class Config extends Component  {
    constructor(props) {
        super(props);
        this.ApiUrl = 'http://localhost:4000/api/';
        this.Url = 'http://localhost:4000/';
    }
}

const config = new Config();
export default config;